﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCuentas.GUI
{
    public class DlgRetirarCuenta : Form
    {
        private ulong[] cuentas;

        public DlgRetirarCuenta(ulong[] cuentas)
        {
            this.cuentas = cuentas;
            this.Build();
        }

        private Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Continuar"
            };

            this.AcceptButton = btGuarda;
            this.CancelButton = btCierra;

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }

        Panel buildCCCP()
        {

            var toret = new Panel { Dock = DockStyle.Top };
            this.edCCC = new ComboBox { Dock = DockStyle.Fill };
            var lbTipo = new Label
            {
                Text = "CCC :",
                Dock = DockStyle.Left
            };

            toret.MaximumSize = new Size(int.MaxValue, edCCC.Height * 2);
            string[] cuentasMostrar = new string[this.cuentas.Length]; 
            for (int i = 0; i < this.cuentas.Length; i++)
            {
                cuentasMostrar[i] = Convert.ToString(this.cuentas[i]);
            }

            edCCC.Items.AddRange(cuentasMostrar);

            if (cuentasMostrar.Length > 0)
            {
                edCCC.SelectedItem = edCCC.Items[0];
            }

            toret.Controls.Add(edCCC);
            toret.Controls.Add(lbTipo);

            Console.WriteLine("EL CCC ELEGIDO ES: " + this.edCCC.Text);
            return toret;
        }

        Panel buildSaldoP()
        {
            var toret = new Panel();
            this.edSaldo = new TextBox
            {
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill,
                Name = "edSaldo"


            };

            var lbSaldo = new Label
            {
                Text = "Importe: ",
                Dock = DockStyle.Left
            };

            this.edSaldo.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.Saldo.ToString());

                if (!double.TryParse(this.edSaldo.Text, out double saldoV))
                {
                    invalid = true;
                }

                if (invalid)
                {
                    this.edSaldo.Text = "Formato inválido";
                    edSaldo.Focus();
                }
                else
                {
                    Saldo = double.Parse(this.edSaldo.Text);
                }

                btAccept.Enabled = !invalid;
                cancelArgs.Cancel = invalid;
            };

            toret.Controls.Add(this.edSaldo);
            toret.Controls.Add(lbSaldo);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edSaldo.Height * 2);

            return toret;
        }
        


        void Build()
        {
            this.SuspendLayout();

            this.panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            this.panelInserta.SuspendLayout();
            this.Controls.Add(this.panelInserta);

            var panelCCC = this.buildCCCP();
            this.panelInserta.Controls.Add(panelCCC);
            var panelSaldo = this.buildSaldoP();
            this.panelInserta.Controls.Add(panelSaldo);

            var panelBotones = this.BuildPnlBotones();
            this.panelInserta.Controls.Add(panelBotones);

            this.panelInserta.ResumeLayout(true);

            this.Text = "Realizar retirada: ";
            this.Size = new Size(400,
                            panelCCC.Height 
                            + panelSaldo.Height + panelBotones.Height + 20);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }
        

        
        private ComboBox edCCC;
        private TextBox edSaldo;
        

        private Panel panelInserta;

        public CheckedListBox listTitulares;
        

        public double Saldo
        {
            get; set;
        }
        public double Interes
        {
            get; set;
        }

        public List<string> claveCliente
        {
            get; set;
        }

        public string CCC => this.edCCC.Text;



    }

}

