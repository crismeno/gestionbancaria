﻿using GestionClientes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCuentas
{
    public abstract class Cuenta
    {
        public Cuenta(ulong ccc, string tipo, double saldo, List<Cliente> titulares, DateTime fechaApertura, double interesMensual, List<Operacion> depositos, List<Operacion> retiradas)
        {
            this.CCC = ccc;
            this.Tipo = tipo;
            this.Saldo = saldo;
            this.Titulares = titulares;
            this.InteresMensual = interesMensual;
            this.FechaApertura = fechaApertura;

            if (depositos != null)
            {
                this.Depositos = depositos;
            }
            else
            {
                this.Depositos = new List<Operacion>();
            }

            if (retiradas != null)
            {
                this.Retiradas = retiradas;
            }
            else
            {
                this.Retiradas = new List<Operacion>(); 
            }

            
        }

        public ulong CCC
        {
            get; private set;
        }

        public string Tipo
        {
            get; private set;
        }

        public double Saldo
        {
            get; set;
        }

        public List<Cliente> Titulares
        {
            get; private set;
        }

        public DateTime FechaApertura
        {
            get; private set;
        }

        public double InteresMensual
        {
            get; private set;
        }

        public List<Operacion> Depositos
        {
            get; private set;
        }

        public List<Operacion> Retiradas
        {
            get; private set;
        }

        
        

    }
}
