﻿using GestionClientes;
using GestionClientes.Core;
using GestionCuentas.Cuentas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GestionCuentas
{
    public class RegistroCuentas
    {

        //Etiquetas XML
        public const string lblCuentas = "cuentas";
        public const string lblCuenta = "cuenta";
        public const string lblCCC = "ccc";
        public const string lblTipo = "tipo";
        public const string lblSaldo = "saldo";
        public const string lblTitulares = "titulares";
        public const string lblTitular = "titular";
        public const string lblFecha = "fechaApertura";
        public const string lblInteres = "interes";
        public const string lblDni = "dni";


        public const string lblDepositos = "depositos";
        public const string lblDeposito = "deposito";
        public const string lblRetiradas = "retiradas";
        public const string lblRetirada = "retirada";
        public const string lblIdOperacion = "idOperacion";
        public const string lblTipoOp = "tipo";
        public const string lblImporte = "importe";
        public const string lblFechaOp = "fecha";

        public const string archivoXML = "cuentas.xml";

        public List<Cuenta> cuentas;

        public RegistroClientes rgClientes;

        public RegistroCuentas()
        {
            this.cuentas = new List<Cuenta>();
            this.rgClientes = RegistroClientes.RecuperarXml();

        }

        public List<Cuenta> cuentasRetirar()
        {
            List<Cuenta> toret = new List<Cuenta>();
            foreach (Cuenta c in this.cuentas)
            {
                if (c.Tipo.Equals("CORRIENTE"))
                {
                    toret.Add(c);
                }
            }
            return toret;
        }

        public int Count => this.cuentas.Count;

        public void Add(Cuenta c)
        {
            this.cuentas.Add(c);
        }

        public void Remove(Cuenta c)
        {
            this.cuentas.Remove(c);
        }

        public Cuenta getCuenta(ulong ccc)
        {
            foreach (Cuenta c in this.cuentas)
            {
                if (c.CCC == ccc)
                {
                    return c;
                }
            }

            return null;
        }

        public Cuenta this[int i]
        {
            get { return this.cuentas[i]; }
            set { this.cuentas[i] = value; }
        }

        public bool comprobarCCC(ulong ccc)
        {
            bool toret = false;
            foreach (Cuenta c in this.cuentas)
            {
                if (c.CCC == ccc)
                {
                    toret = true;
                }
            }

            return toret;
        }
        public List<Cuenta> List
        {
            get { return this.cuentas; }
        }

        public void toXML()
        {
            toXML(archivoXML);
        }

        public void toXML(string fichero)
        {
            var doc = new XDocument();
            var root = new XElement(lblCuentas);

            foreach (Cuenta cuenta in this.cuentas)
            {
                XElement ccc = new XElement(lblCCC, cuenta.CCC);
                XElement tipo = new XElement(lblTipo, cuenta.Tipo);
                XElement saldo = new XElement(lblSaldo, cuenta.Saldo);

                XElement titulares = new XElement(lblTitulares);
                if (cuenta.Titulares != null)
                {
                    foreach (Cliente cliente in cuenta.Titulares)
                    {
                        XElement titular = new XElement(lblTitular);
                        XAttribute dni = new XAttribute(lblDni, cliente.DNI);

                        titular.Add(dni);
                        titulares.Add(titular);
                    }
                }

                XElement fecha = new XElement(lblFecha, cuenta.FechaApertura);
                XElement interes = new XElement(lblInteres, cuenta.InteresMensual);

                XElement depositos = new XElement(lblDepositos);

                if (cuenta.Depositos.Count > 0)
                {
                    foreach (Operacion op in cuenta.Depositos)
                    {
                        XElement deposito = new XElement(lblDeposito);
                        
                        XElement tipoOp = new XElement(lblTipoOp, op.Tipo);
                        XElement importe = new XElement(lblImporte, op.Importe);
                        XElement fechaOp = new XElement(lblFechaOp, op.Fecha);
                        
                        deposito.Add(tipoOp);
                        deposito.Add(importe);
                        deposito.Add(fechaOp);

                        depositos.Add(deposito);
                    }
                }

                XElement retiradas = new XElement(lblRetiradas);

                if (cuenta.Retiradas.Count > 0)
                {
                    foreach (Operacion op in cuenta.Retiradas)
                    {
                        XElement retirada = new XElement(lblRetirada);
                        
                        XElement tipoOp = new XElement(lblTipoOp, op.Tipo);
                        XElement importe = new XElement(lblImporte, op.Importe);
                        XElement fechaOp = new XElement(lblFechaOp, op.Fecha);
                        
                        retirada.Add(tipoOp);
                        retirada.Add(importe);
                        retirada.Add(fechaOp);

                        retiradas.Add(retirada);
                    }
                }

                XElement cuentaXML = new XElement(lblCuenta);

                cuentaXML.Add(ccc);
                cuentaXML.Add(tipo);
                cuentaXML.Add(saldo);
                cuentaXML.Add(titulares);
                cuentaXML.Add(fecha);
                cuentaXML.Add(interes);
                cuentaXML.Add(depositos);
                cuentaXML.Add(retiradas);

                root.Add(cuentaXML);

            }

            doc.Add(root);
            doc.Save(fichero);
        }

        public RegistroCuentas fromXML()
        {
            return fromXML(archivoXML);
        }

        public void Clear()
        {
            this.cuentas.Clear();
        }

        public RegistroCuentas fromXML(string fichero)
        {
            Console.WriteLine("HE LLEGADO AQUI");

            var toret = new RegistroCuentas();
            try
            {
                var doc = XDocument.Load(fichero);

                if (doc.Root != null && doc.Root.Name == lblCuentas)
                {
                    Console.WriteLine("Voy a empezar con el XML!");
                    var cuentas = doc.Root.Elements(lblCuenta);
                    foreach (XElement cuenta in cuentas)
                    {
                        Console.WriteLine("Comenzaré a insertar una cuenta!");

                        ulong ccc = (ulong)cuenta.Element(lblCCC);
                        string tipo = (string)cuenta.Element(lblTipo);
                        double saldo = (double)cuenta.Element(lblSaldo);
                        List<Cliente> titulares = this.getTitulares(cuenta.Element(lblTitulares));
                        DateTime fechaApertura = (DateTime)cuenta.Element(lblFecha);
                        double interes = (double)cuenta.Element(lblInteres);
                        List<Operacion> depositos = this.getOperaciones(cuenta.Element(lblDepositos));
                        List<Operacion> retiradas = this.getOperaciones(cuenta.Element(lblRetiradas));

                        switch (tipo)
                        {
                            case "AHORRO":
                                Ahorro a = new Ahorro(ccc, tipo, saldo, titulares, fechaApertura, interes, depositos, retiradas);
                                toret.Add(a);
                                Console.WriteLine("Se ha insertado una cuenta de tipo ahorro");
                                break;

                            case "CORRIENTE":
                                Corriente c = new Corriente(ccc, tipo, saldo, titulares, fechaApertura, interes, depositos, retiradas);
                                toret.Add(c);
                                Console.WriteLine("Se ha insertado una cuenta de tipo corriente");

                                break;

                            case "VIVIENDA":
                                Vivienda v = new Vivienda(ccc, tipo, saldo, titulares, fechaApertura, interes, depositos, retiradas);
                                toret.Add(v);
                                Console.WriteLine("Se ha insertado una cuenta de tipo vivienda");

                                break;

                        }

                    }
                }
            }
            catch (XmlException)
            {
                toret.Clear();
            }
            catch (IOException)
            {
                toret.Clear();
            }

            return toret;

        }

        private List<Cliente> getTitulares(XElement titulares)
        {
            List<Cliente> toret = new List<Cliente>();

            foreach (XElement titular in titulares.Elements())
            {
                Cliente c = this.rgClientes.getCliente((string)titular.Attribute(lblDni));
                toret.Add(c);
            }

            return toret;
        }

        private List<Operacion> getOperaciones(XElement operaciones)
        {
            List<Operacion> toret = new List<Operacion>();

            foreach (XElement op in operaciones.Elements())
            {
                string tipoOp = (string)op.Element(lblTipoOp);
                double importe = (double)op.Element(lblImporte);
                DateTime fechaOp = (DateTime)op.Element(lblFechaOp);

                Operacion operacion = new Operacion( tipoOp, importe, fechaOp);
                toret.Add(operacion);
            }

            return toret;
        }


    }
}
