﻿using GestionCuentas.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCuentas
{
    class Program
    {
        public static void Main(string[] args)
        {
            var form = new MainWindowCore().mwv;
            Application.Run(form);
        }
    }
}
