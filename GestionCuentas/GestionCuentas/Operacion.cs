﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCuentas
{
    public class Operacion
    {
        public Operacion(string tipo, double importe, DateTime fecha)
        {
            this.Tipo = tipo;
            this.Importe = importe;
            this.Fecha = fecha;
        }

        public string Tipo
        {
            get; private set;
        }

        public double Importe
        {
            get; private set;
        }

        public DateTime Fecha
        {
            get; private set;
        }

    }
}
