﻿using GestionClientes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCuentas.Cuentas
{
   public  class Ahorro : Cuenta
    {

        public Ahorro(ulong ccc, string tipo, double saldo, List<Cliente> titulares, DateTime fechaApertura, double interesMensual, List<Operacion> depositos, List<Operacion> retiradas) : base(ccc, tipo, saldo, titulares, fechaApertura, interesMensual, depositos, retiradas)
        {
        }
        
    }
}
