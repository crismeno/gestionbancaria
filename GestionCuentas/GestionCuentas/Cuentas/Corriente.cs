﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCuentas.Cuentas
{
    using GestionClientes.Core;

    public class Corriente : Cuenta
    {
        public Corriente(ulong ccc, string tipo, double saldo, List<Cliente> titulares, DateTime fechaApertura, double interesMensual, List<Operacion> depositos, List<Operacion> retiradas) : base(ccc, tipo, saldo, titulares, fechaApertura, interesMensual, depositos, retiradas)
        {
        }

        public void retirar(double cantidad)
        {
            Operacion ret = new Operacion("RET", cantidad, DateTime.Now);
            //Añadimos el depósito al registro
            this.Retiradas.Add(ret);
            //Actualizamos el saldo de la cuenta
            this.Saldo += cantidad;
        }
    }
}
