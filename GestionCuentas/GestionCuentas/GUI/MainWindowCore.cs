﻿using GestionClientes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCuentas.Cuentas;
namespace GestionCuentas.GUI
{
    class MainWindowCore
    {
        public MainWindowView mwv;
        public RegistroCuentas rg;
        //public RegistroClientes rg_clientes; --> NECESARIO GESTION DE CLIENTES
        

        public MainWindowCore()
        {
            rg = new RegistroCuentas().fromXML();
            //rg_clientes = new RegistroClientes().fromXML(); --> NECESARIO GESTION DE CLIENTES

            this.mwv = new MainWindowView();
            this.mwv.Shown += (sender, e) => this.Actualiza();
            this.mwv.gridLista.Click += (sender, e) => this.ClickLista();
            this.mwv.OpGuardar.Click += (sender, e) => this.Guardar();
            this.mwv.OpAnadir.Click += (sender, e) => this.Insertar();
        }

        /*
         * FUNCIONES BOTONES PANELINIT
         */

        void Guardar()
        {
            this.rg.toXML();
        }

        void ClickLista()
        {
            if(this.mwv.gridLista.CurrentCell.ColumnIndex == 6)
            {
                this.Eliminar();
            }
            else if(this.mwv.gridLista.CurrentCell.ColumnIndex == 7)
            {
                int fila = this.mwv.gridLista.CurrentCell.RowIndex;
                this.Modificar((ulong) this.mwv.gridLista.Rows[fila].Cells[0].Value);
            }
        }
        

        void Eliminar()
        {
            ulong cccEliminar = (ulong) this.mwv.gridLista.CurrentRow.Cells[0].Value;

            //Dialogo de confirmación de eiminación
            DialogResult result;
            string mensaje = "¿Está seguro de que desea eliminar la cuenta con CCC( " +
                            cccEliminar + " ), del registro de cuentas?";
            string tittle = "Eliminar cuenta";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            result = MessageBox.Show(mensaje, tittle, buttons);

            if (result == DialogResult.Yes)
            {
                rg.Remove(rg.getCuenta(cccEliminar));
                this.Actualiza();
            }
            
        }

        void Modificar(ulong ccc)
        {

            //A partir de la clave de la entidad Cuenta, obtenemos la cuenta a modificar
            Cuenta mod = this.rg.getCuenta(ccc);
            string tipo = mod.Tipo;
            List<Cliente> titulares = mod.Titulares;


            var dlgModificar = new DlgModificarCuenta(mod);
            if (dlgModificar.ShowDialog() == DialogResult.OK)
            {
                this.rg.Remove(mod);

                double saldo = dlgModificar.Saldo;
                double interes = dlgModificar.Interes;

                Cuenta c = null;

                switch (tipo)
                {
                    case "CORRIENTE":
                        c = new Corriente(ccc,tipo,saldo,mod.Titulares,mod.FechaApertura,interes,mod.Depositos,mod.Retiradas);
                        break;
                    case "VIVIENDA":
                        c = new Vivienda(ccc, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;
                    case "AHORRO":
                        c = new Ahorro(ccc, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;

                }
                this.rg.Add(c);
                
            }
            this.Actualiza();
        }

        void Insertar()
        {
            var dlgInsertar = new DlgInsertarCuenta(this.rg.rgClientes);
            
            if (dlgInsertar.ShowDialog() == DialogResult.OK)
            {
                ulong ccc = dlgInsertar.CCC;
                if (this.rg.comprobarCCC(ccc))
                {
                    string mensaje = "No se ha insertado la cuenta, porque el CCC ya pertenece a una cuenta del registro";
                    string tittle = "Error al insertar";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    var result = MessageBox.Show(mensaje, tittle, buttons);
                }
                else
                {

                    string tipo = dlgInsertar.Tipo;
                    DateTime fecha = DateTime.Now;

                    double saldo = dlgInsertar.Saldo;
                    double interes = dlgInsertar.Interes;

                    //Obtenemos los clientes seleccionados en el checkedlistbox
                    //List<Cliente> titulares = this.getTitulares(dlgInsertar.claveCliente);

                    Cuenta c = null;

                    switch (tipo)
                    {
                        case "CORRIENTE":
                            c = new Corriente(ccc, tipo, saldo, null, fecha, interes, null, null);
                            break;
                        case "VIVIENDA":
                            c = new Vivienda(ccc, tipo, saldo, null, fecha, interes, null, null);
                            break;
                        case "AHORRO":
                            c = new Ahorro(ccc, tipo, saldo, null, fecha, interes, null, null);
                            break;

                    }
                    this.rg.Add(c);
                }


            }
            this.Actualiza();
        }

        //Función para obtener los titulares de una cuenta a partir de sus claves obtenidas en el checkedlistbox --> NECESARIO GESTIÓN DE CLIENTES
        List<Cliente> getTitulares(List<string> clavesTitulares)
        {
            List<Cliente> toret = new List<Cliente>();

            foreach (string clave in clavesTitulares)
            {
                //Cliente c = rg_clientes.getCliente(clave);
                //toret.Add(c);
            }

            return toret;
        }


        void Actualiza()
        {
            int numElementos = this.rg.Count;
            for (int i = 0; i < numElementos; i++)
            {
                if (this.mwv.gridLista.Rows.Count <= i)
                {
                    this.mwv.gridLista.Rows.Add();
                }
                this.ActuaizarFila(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.mwv.gridLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.mwv.gridLista.Rows.RemoveAt(numElementos);
            }

            
        }

        private void ActuaizarFila(int numFila)
        {
            if (numFila < 0
              || numFila > this.mwv.gridLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.mwv.gridLista.Rows[numFila];
            Cuenta cuenta = this.rg[numFila];

            StringBuilder titulares = new StringBuilder();
            if (cuenta.Titulares != null)
            {
                foreach (Cliente cli in cuenta.Titulares)
                {
                    titulares.Append(cli.Nombre + " - ");
                }
            }
            

            fila.Cells[0].Value = cuenta.CCC;
            fila.Cells[1].Value = cuenta.Tipo;
            fila.Cells[2].Value = cuenta.Saldo;
            fila.Cells[3].Value = cuenta.InteresMensual;
            fila.Cells[4].Value = cuenta.FechaApertura;
            fila.Cells[5].Value = titulares.ToString();
            fila.Cells[6].Value = "-";
            fila.Cells[7].Value = "+";


        }
        
    }
}
