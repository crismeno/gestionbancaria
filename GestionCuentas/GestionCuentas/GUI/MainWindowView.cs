﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace GestionCuentas.GUI
{
   public class MainWindowView : Form
    {
        public DataGridView gridLista;
        public Panel panelPpal;

        //Menú contextual de los elementos del gridView
        public ContextMenu cntxMenuListElements;

        public MenuItem editItem;
        public MenuItem removeItem;

        public MenuItem opMovimientos;
        public MenuItem opRetirar;
        public MenuItem opDepositar;


        public MainWindowView()
        {
            
            this.panelPpal = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill
            };
            this.BuildMenu();
            Panel panelLista = BuildPanelListCuentas();
            this.panelPpal.Controls.Add(panelLista);       

            this.panelPpal.SuspendLayout();
            this.Controls.Add(this.panelPpal);
            this.panelPpal.Controls.Add(panelLista);
            this.panelPpal.ResumeLayout(false);

            this.MinimumSize = new Size(930, 100);
            this.Text = "Gestión de cuentas DIA";
            this.Resize += (obj, e) => this.ResizeWindow();
            this.Font = new System.Drawing.Font("Century Gothic", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            this.ResizeWindow();
            this.ResumeLayout(true);
        }

       
        
        //Panel que muestra todas las cuentas registradas
        Panel BuildPanelListCuentas()
        {
            Panel toret = new Panel()
            {
                Dock = DockStyle.Fill
            };

            this.gridLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };

            this.gridLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.gridLista.ColumnHeadersDefaultCellStyle.BackColor = Color.PeachPuff;

            var gridCelda = new DataGridViewTextBoxCell();
            var gridCeldaButton = new DataGridViewButtonCell();


            //Diseño de las celdas del grid de la lista
            gridCelda.Style.BackColor = Color.Moccasin;
            gridCelda.Style.ForeColor = Color.Black;
                
            gridCelda.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //Diseño de las celdas de botones
            gridCeldaButton.Style.BackColor = Color.White;
            gridCeldaButton.Style.ForeColor = Color.Black;


            var c1 = new DataGridViewTextBoxColumn
            {

                CellTemplate = gridCelda,
                HeaderText = "CCC Cuenta",
                Width = 100,
                ReadOnly = true

            };

            var c2 = new DataGridViewTextBoxColumn
            {
                CellTemplate = gridCelda,
                HeaderText = "Tipo",
                Width = 100,
                ReadOnly = true

            };

            var c3 = new DataGridViewTextBoxColumn
            {
                CellTemplate = gridCelda,
                HeaderText = "Saldo",
                Width = 100,
                ReadOnly = true

            };

            var c4 = new DataGridViewTextBoxColumn
            {
                CellTemplate = gridCelda,
                HeaderText = "Interes %",
                Width = 100,
                ReadOnly = true

            };

            var c5 = new DataGridViewTextBoxColumn
            {
                CellTemplate = gridCelda,
                HeaderText = "Fecha creación",
                Width = 100,
                ReadOnly = true

            };

            var c6 = new DataGridViewTextBoxColumn
            {
                CellTemplate = gridCelda,
                HeaderText = "Titulares",
                Width = 100,
                ReadOnly = true

            };

            var c7 = new DataGridViewButtonColumn
            {
                CellTemplate = gridCeldaButton,
                HeaderText = "Eliminar",
                Width = 100,
                ReadOnly = true

            };

            var c8 = new DataGridViewButtonColumn
            {
                CellTemplate = gridCeldaButton,
                HeaderText = "Modificar",
                Width = 100,
                ReadOnly = true

            };
            this.gridLista.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.gridLista.Columns.AddRange(new DataGridViewColumn[] { c1, c2, c3, c4, c5, c6, c7, c8 });

            toret.Controls.Add(this.gridLista);

            return toret;
        }

        private void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.panelPpal.ClientRectangle.Width;
            int heigh = this.panelPpal.ClientRectangle.Height;

            // Redimensionar la tabla
            this.gridLista.Width = width;

            this.gridLista.Height = heigh;

            this.gridLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .13);
            this.gridLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .14);
            this.gridLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .14);
            this.gridLista.Columns[3].Width =
                                (int)System.Math.Floor(width * .10);
            this.gridLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .14);
            this.gridLista.Columns[5].Width =
                                (int)System.Math.Floor(width * .15);
            this.gridLista.Columns[6].Width =
                                (int)System.Math.Floor(width * .10);
            this.gridLista.Columns[7].Width =
                                (int)System.Math.Floor(width * .10);
        }

        void BuildMenu()
        {
            this.Mpal = new MainMenu();

            this.MArchivo = new MenuItem("Archivo");
            this.MEditar = new MenuItem("Insertar");
            this.opMovimientos = new MenuItem("Movimientos");

            this.OpGuardar = new MenuItem("Guardar");
            this.OpSalir = new MenuItem("Salir");
            this.OpAnadir = new MenuItem("Añadir cuenta");

            this.opRetirar = new MenuItem("Retirar de cuenta");
            this.opDepositar = new MenuItem("Depositar en cuenta");


            this.MArchivo.MenuItems.Add(this.OpGuardar);

            this.MArchivo.MenuItems.Add(this.OpSalir);

            this.MEditar.MenuItems.Add(this.OpAnadir);


            this.opMovimientos.MenuItems.Add(this.opRetirar);
            this.opMovimientos.MenuItems.Add(this.opDepositar);

            this.Mpal.MenuItems.Add(MArchivo);
            this.Mpal.MenuItems.Add(MEditar);
            this.Mpal.MenuItems.Add(this.opMovimientos);

            this.Menu = Mpal;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MainWindowView
            // 
            this.ClientSize = new System.Drawing.Size(500, 550);
            this.Name = "MainWindowView";
            this.ResumeLayout(false);

        }
        
       

        public MainMenu Mpal { get; set; }
        public MenuItem MArchivo { get; set; }
        public MenuItem MEditar { get; set; }
        public MenuItem OpAnadir { get; set; }
        public MenuItem OpSalir { get; set; }
        public MenuItem OpGuardar { get; set; }



    }
}
