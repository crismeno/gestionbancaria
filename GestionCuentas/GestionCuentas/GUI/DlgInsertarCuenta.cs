﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionClientes.Core;

namespace GestionCuentas.GUI
{
   public class DlgInsertarCuenta : Form
    {
        public RegistroClientes rgCli;

        public DlgInsertarCuenta(RegistroClientes rgCli)
        {
            this.rgCli = RegistroClientes.RecuperarXml();
            this.Build();
        }

        private Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Añadir"
            };

            this.AcceptButton = btGuarda;
            this.CancelButton = btCierra;

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }

        Panel buildCCCP()
        {
            var toret = new Panel();
            this.edCCC = new TextBox
            {
                Dock = DockStyle.Fill,
                Name = "edCCC"
            };

            var lbCCC = new Label
            {
                Text = "CCC: ",
                Dock = DockStyle.Left
            };



            this.edCCC.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.CCC.ToString());
                
                if (!ulong.TryParse(this.edCCC.Text, out ulong cccV))
                {
                    invalid = true;
                }

                if (invalid)
                {
                    this.edCCC.Text = "Formato inválido";
                    edCCC.Focus();
                }
                else
                {
                    CCC = ulong.Parse(this.edCCC.Text);
                }

                btAccept.Enabled = !invalid;
                cancelArgs.Cancel = invalid;
            };

            toret.Controls.Add(this.edCCC);
            toret.Controls.Add(lbCCC);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edCCC.Height * 2);

            return toret;
        }

        Panel buildTipoP()
        {
        
            var toret = new Panel { Dock = DockStyle.Top };
            this.edTipo = new ComboBox { Dock = DockStyle.Fill };
            var lbTipo = new Label
            {
                Text = "Tipo :",
                Dock = DockStyle.Left
            };

            toret.MaximumSize = new Size(int.MaxValue, edTipo.Height * 2);
            edTipo.Items.AddRange(new string[] { "VIVIENDA", "AHORRO", "CORRIENTE" }); 

            edTipo.SelectedItem = edTipo.Items[0];

            toret.Controls.Add(edTipo);
            toret.Controls.Add(lbTipo);


            return toret;
        }

        Panel buildTitularesP()
        {
            var toret = new TableLayoutPanel()
            {
                RowCount = 1,
                ColumnCount = 2
            };

            var lbTitulares = new Label
            {
                Dock = DockStyle.Fill,
                Text = "Titulares: ",
            };

            this.listTitulares = new CheckedListBox();

            //Creamos un checkedListBox con todos los clientes que pueden ser titulares
            string[] op = new string[this.rgCli.Count];
            for (int i=0;i<op.Length ;i++)
            {
                Cliente cliente = this.rgCli[i];
                op[i] = cliente.DNI;
            }
            listTitulares.Items.AddRange(op);

            

            toret.Controls.Add(lbTitulares);
            toret.Controls.Add(listTitulares);

            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, listTitulares.Height * 2);

            return toret;
        }

        Panel buildSaldoP()
        {
            var toret = new Panel();
            this.edSaldo = new TextBox
            {
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill,
                Name = "edSaldo"


            };

            var lbSaldo = new Label
            {
                Text = "Saldo: ",
                Dock = DockStyle.Left
            };

            this.edSaldo.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.Saldo.ToString());

                if (!double.TryParse(this.edSaldo.Text, out double saldoV))
                {
                    invalid = true;
                }

                if (invalid)
                {
                    this.edSaldo.Text = "Formato inválido";
                    edSaldo.Focus();
                }
                else
                {
                    Saldo = double.Parse(this.edSaldo.Text);
                }

                btAccept.Enabled = !invalid;
                cancelArgs.Cancel = invalid;
            };

            toret.Controls.Add(this.edSaldo);
            toret.Controls.Add(lbSaldo);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edSaldo.Height * 2);

            return toret;
        }

        Panel buildInteresP()
        {
            var toret = new Panel();
            this.edInteres = new TextBox
            {
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill,
                Name = "edInteres"

            };

            var lbInteres = new Label
            {
                Text = "Interes: ",
                Dock = DockStyle.Left
            };

            this.edInteres.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.edInteres.Text.ToString());
                if (!double.TryParse(this.edInteres.Text, out double interesV))
                {
                    invalid = true;
                }

                if (invalid)
                {
                    this.edInteres.Text = "Formato inválido";
                    edInteres.Focus();
                }
                else
                {
                    Interes = double.Parse(this.edInteres.Text);
                }

                btAccept.Enabled = !invalid;
                cancelArgs.Cancel = invalid;
            };

            toret.Controls.Add(this.edInteres);
            toret.Controls.Add(lbInteres);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edInteres.Height * 2);

            return toret;
        }


        void Build()
        {
            this.SuspendLayout();

            this.panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            this.panelInserta.SuspendLayout();
            this.Controls.Add(this.panelInserta);

            var panelCCC = this.buildCCCP();
            this.panelInserta.Controls.Add(panelCCC);
            var panelTipo = this.buildTipoP();
            this.panelInserta.Controls.Add(panelTipo);
            var panelTitulares = this.buildTitularesP();
            this.panelInserta.Controls.Add(panelTitulares);
            var panelSaldo = this.buildSaldoP();
            this.panelInserta.Controls.Add(panelSaldo);
            var panelInteres = this.buildInteresP();
            this.panelInserta.Controls.Add(panelInteres);


            var panelBotones = this.BuildPnlBotones();
            this.panelInserta.Controls.Add(panelBotones);

            this.panelInserta.ResumeLayout(true);

            this.Text = "Añadir cuenta";
            this.Size = new Size(400,
                            panelCCC.Height + panelTipo.Height
                            + panelSaldo.Height + panelInteres.Height + panelTitulares.Height + panelBotones.Height + 20);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        public bool validaciones()
        {
            var toret = true;
            
            foreach (Panel panel in this.panelInserta.Controls)
            {
                foreach (Control item in panel.Controls)
                {
                    if (item is TextBox)
                    {
                        if (item.Text == "")
                        {
                            MessageBox.Show("Complete todos los campos del formulario antes de continuar");
                            item.Focus();
                            toret = false;

                        }
                        if (item.Name == "edSaldo" || item.Name == "edInteres")
                        {
                            string saldo = edSaldo.Text;
                            string interes = edInteres.Text;

                            if (!double.TryParse(saldo, out double saldoOK))
                            {
                                MessageBox.Show("El contenido del campo tiene que estar en formato decimal");
                                item.Focus();
                                toret = false;

                            }
                            if (!double.TryParse(interes, out double interesOK))
                            {
                                MessageBox.Show("El contenido del campo tiene que estar en formato decimal");
                                item.Focus();
                                toret = false;

                            }
                        }
                    }
                    else if (item is ComboBox)
                    {
                        if (item.Text == "")
                        {
                            MessageBox.Show("Debe ser seleccionado algún elemento");
                            item.Focus();
                            toret = false;


                        }
                    }
                }
                
            }

            return toret;
        }
        

        private TextBox edCCC;
        private ComboBox edTipo;
        private TextBox edNumTitulares;
        private TextBox edSaldo;
        private TextBox edInteres;

        private Button btVerTitulares;

        private Panel panelInserta;

        public CheckedListBox listTitulares;

        public ulong CCC
        {
            get;set;
        }

        public double Saldo
        {
            get; set;
        }
        public double Interes
        {
            get; set;
        }

        public List<string> claveCliente
        {
            get;set;
        }


        public string Tipo => this.edTipo.Text;
    }
}
