﻿using GestionClientes.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionCuentas.GUI
{
    public class DlgModificarCuenta : Form
    {
        private Cuenta c;
        public RegistroClientes rgCli;

        public DlgModificarCuenta(Cuenta c)
        {
            this.rgCli = RegistroClientes.RecuperarXml();
            this.c = c;
            this.Build();
        }

        private Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Editar"
            };

            this.AcceptButton = btGuarda;
            this.CancelButton = btCierra;

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }

        Panel buildCCCP()
        {
            var toret = new Panel();
            this.edCCC = new TextBox
            {
                TextAlign = HorizontalAlignment.Center,
                Text = System.Convert.ToString(this.c.CCC) ,
                Dock = DockStyle.Fill,
                ReadOnly = true
            };

            var lbCCC = new Label
            {
                Text = "CCC: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edCCC);
            toret.Controls.Add(lbCCC);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edCCC.Height * 2);

            return toret;
        }

        Panel buildTipoP()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edTipo = new TextBox {
                TextAlign = HorizontalAlignment.Center,
                Dock = DockStyle.Fill,
                Text = this.c.Tipo,
                ReadOnly = true
            };
            var lbTipo = new Label
            {
                Text = "Tipo:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edTipo);
            toret.Controls.Add(lbTipo);
            toret.MaximumSize = new Size(int.MaxValue, edTipo.Height * 2);
            

            return toret;
        }

        Panel buildTitularesP()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 3,
                RowCount = 1
            };


            var lbTitulares = new Label
            {
                Dock = DockStyle.Fill,
                Text = "Titulares: ",
            };
            var listTitulares = new CheckedListBox();
            //Creamos un checkedListBox con todos los clientes que pueden ser titulares
            string[] op = new string[this.rgCli.Count];
            for (int i = 0; i < op.Length; i++)
            {
                Cliente cliente = this.rgCli[i];
                op[i] = cliente.DNI;
            }
            listTitulares.Items.AddRange(op);

            if (this.c.Titulares != null)
            {
                foreach (Cliente c in this.c.Titulares)
                {
                    for (int i = 0; i < op.Length; i++)
                    {
                        if (c.DNI == op[i])
                        {
                            listTitulares.SetItemChecked(i, true);
                        }
                    }
                }
            }

            toret.Controls.Add(lbTitulares);    
            toret.Controls.Add(listTitulares);


            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, listTitulares.Height * 2);

            return toret;
        }

        Panel buildSaldoP()
        {
            var toret = new Panel();
            this.edSaldo = new TextBox
            {
                Text = System.Convert.ToString(this.c.Saldo),
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill
                

            };

            var lbSaldo = new Label
            {
                Text = "Saldo: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edSaldo);
            toret.Controls.Add(lbSaldo);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edSaldo.Height * 2);

            return toret;
        }

        Panel buildInteresP()
        {
            var toret = new Panel();
            this.edInteres = new TextBox
            {
                Text = Convert.ToString(this.c.InteresMensual),
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill,

            };

            var lbInteres = new Label
            {
                Text = "Interes: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edInteres);
            toret.Controls.Add(lbInteres);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edInteres.Height * 2);

            return toret;
        }


        void Build()
        {
            this.SuspendLayout();

            var panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            panelInserta.SuspendLayout();
            this.Controls.Add(panelInserta);

            var panelCCC = this.buildCCCP();
            panelInserta.Controls.Add(panelCCC);
            var panelTipo = this.buildTipoP();
            panelInserta.Controls.Add(panelTipo);
            var panelTitulares = this.buildTitularesP();
            panelInserta.Controls.Add(panelTitulares);
            var panelSaldo = this.buildSaldoP();
            panelInserta.Controls.Add(panelSaldo);
            var panelInteres = this.buildInteresP();
            panelInserta.Controls.Add(panelInteres);


            var panelBotones = this.BuildPnlBotones();
            panelInserta.Controls.Add(panelBotones);

            panelInserta.ResumeLayout(true);

            this.Text = "Editar cuenta";
            this.Size = new Size(400,
                            panelCCC.Height + panelTipo.Height
                            + panelSaldo.Height + panelInteres.Height + panelTitulares.Height + panelBotones.Height + 20);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        private TextBox edCCC;
        private TextBox edTipo;
        private TextBox edSaldo;
        private TextBox edInteres;


        
        public double Saldo => System.Convert.ToDouble(this.edSaldo.Text);
        public double Interes => System.Convert.ToDouble(this.edInteres.Text);
        
    }
}
