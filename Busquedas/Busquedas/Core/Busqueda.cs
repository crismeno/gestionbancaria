﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Busquedas.Core
{
    using GestionClientes.Core;
    using GestionCuentas;
    using GestionTransferencias.Core;
    public class Busqueda
    {
        RegistroClientes Clientes;
        RegistroCuentas Cuentas;
        RegistroTransferencias Transferencias;

        public Busqueda(RegistroClientes rcl, RegistroCuentas rcu, RegistroTransferencias rt)
        {
            this.Clientes = rcl;
            this.Cuentas = rcu;
            this.Transferencias = rt;
        }
       
        //==================================================
        /// <summary>
        /// Busca todos los productos que tiene un cliente
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        public void BuscarProductos(string dni)
        {
            Cliente client = GetClient(dni);

        }
        
        //==================================================
        /// <summary>
        /// Busca los movimientos realizados por un cliente
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        public List<Cuenta> BuscarCuentas(string dni)
        {
            var cuentasClient = GetCuentasClient(dni);
            return cuentasClient;
        }

        //==================================================
        /// <summary>
        /// Busca las transferencias realizadas por un cliente
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        public List<Transferencia> BuscarTransferencias(string dni)
        {
            Cliente client = GetClient(dni);
            //Busca las cuentas del cliente
            var cuentasClient = GetCuentasClient(dni);

            //retorna de transferencias del cliente
            return GetTransfClient(cuentasClient);
        }

        public List<Operacion> BuscarDepositos(string dni)
        {
            Cliente client = GetClient(dni);
            //Busca las cuentas del cliente
            var cuentasClient = GetCuentasClient(dni);

            //retorna de transferencias del cliente
            return GetDepositosCliente(cuentasClient);
        }

        public List<Operacion> BuscarRetiradas(string dni)
        {
            Cliente client = GetClient(dni);
            //Busca las cuentas del cliente
            var cuentasClient = GetCuentasClient(dni);

            //retorna de transferencias del cliente
            return GetretiradasCliente(cuentasClient);
        }


        //==================================================
        /// <summary>
        /// Busca las transferencias realizadas con una cuenta
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        public List<Transferencia> BuscarTransferenciasCuenta(Cuenta cuenta)
        {
            var transferClient = new List<Transferencia>();

            ulong ccc = cuenta.CCC;
            foreach (Transferencia transferencia in this.Transferencias.List)
            {
                //Si la ccc (orgen o destino) coincide con la cuente del cliente
                if ((ccc.Equals(transferencia.CCCdestino.CCC))
                    || (ccc.Equals(transferencia.CCCorigen.CCC)))
                {
                    transferClient.Add(transferencia);
                }
            }
            return transferClient;
        }
        //==================================================
        /// <summary>
        /// Busca todas las transferencias del banco
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        public List<Transferencia> BuscarTransferenciasBanco()
        {
            return (from tr in this.Transferencias.List
                    select tr).ToList();
        }
        
        //==================================================
        /// <summary>
        /// Devuelve las transferencias de un cliente
        /// </summary>
        /// <param name="cuentas"></param>
        //==================================================
        private List<Transferencia> GetTransfClient(List<Cuenta> cuentas)
        {
            var transferClient = new List<Transferencia>();

            foreach (Cuenta cuenta in cuentas)
            {
                ulong ccc = cuenta.CCC;
                foreach (Transferencia transferencia in this.Transferencias.List)
                {
                    //Si la ccc (orgen o destino) coincide con la cuente del cliente
                    if ((ccc.Equals(transferencia.CCCdestino.CCC))
                        || (ccc.Equals(transferencia.CCCorigen.CCC)))
                    {
                        transferClient.Add(transferencia);
                    }
                }
            }
            return transferClient;
        }

        private List<Operacion> GetDepositosCliente(List<Cuenta> cuentas)
        {
            var depositosLista = new List<Operacion>();

            foreach (Cuenta cuenta in cuentas)
            {
                ulong ccc = cuenta.CCC;
                List<Operacion> depositosCuenta = cuenta.Depositos;
                foreach (Operacion op in depositosCuenta)
                {
                    System.Console.WriteLine("HE ENCONTRADO UN DEPOSITO");
                    depositosLista.Add(op);
                }
            }
            return depositosLista;
        }

        private List<Operacion> GetretiradasCliente(List<Cuenta> cuentas)
        {
            var retiradasLista = new List<Operacion>();

            foreach (Cuenta cuenta in cuentas)
            {
                ulong ccc = cuenta.CCC;
                List<Operacion> retiradasCuenta = cuenta.Retiradas;
                foreach (Operacion op in retiradasCuenta)
                {
                    retiradasLista.Add(op);
                }
            }
            return retiradasLista;
        }
        

        //==================================================
        /// <summary>
        /// Devuelve las cuentas de un cliente
        /// </summary>
        /// <param name="dni"></param>
        //==================================================
        private List<Cuenta> GetCuentasClient(string dni)
        {
            var cuentasClient = new List<Cuenta>();
            //Busca las cuentas del cliente
            foreach (Cuenta cuenta in this.Cuentas.List)
            {
                foreach (Cliente c in cuenta.Titulares)
                {
                    if (c.DNI.Equals(dni))
                    {
                        cuentasClient.Add(cuenta);
                    }
                }
            }
            return cuentasClient;
        }
        
        //==================================================
        /// <summary>
        /// Devuelve un cliente a partir de su dni
        /// </summary>
        /// <param name="dni"></param>
        /// <returns></returns>
        //==================================================
        public Cliente GetClient(string dni)
        {
            var query = (from cl in this.Clientes.List
                         where cl.DNI.Equals(dni)
                         select cl).ToList();
            return query[0];
        }
    }
}
