﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Busquedas.GUI
{
    using Busquedas.Core;
    using Busquedas.GUI.Dlgs;

    using GestionTransferencias.Core;
    using GestionClientes.Core;
    using GestionCuentas;

    class MainWindowCore
    {
        private DlgMovimientos DlgMovimientos;
        private DlgProductos DlgProductos;
        public MainWindowCore()
        {
            this.MainWindowView = new MainWindowView();
            var Cuentas_Reg = new RegistroCuentas();
            this.Cuentas = Cuentas_Reg.fromXML();
            this.Clientes = RegistroClientes.RecuperarXml();
            this.Transferencias = RegistroTransferencias.RecuperarXml(Cuentas_Reg);

            this.Search = new Busqueda(this.Clientes, this.Cuentas, this.Transferencias);
            this.MainWindowView.FormClosed += (sender, e) => this.OnQuit();
            this.MainWindowView.opSalir.Click += (sender, e) => this.Salir();
            this.MainWindowView.mSearch.Click += (sender, e) => this.BuildSearch();

        }
        private void ClickListaMovimientos()
        {

            if (this.DlgMovimientos.GrdLista.CurrentCell.ColumnIndex == 7)
            {
                int row = (int)this.DlgMovimientos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];

                var dlgOp = new DlgOperaciones(cuenta, this.Cliente);
                this.DlgOpen = dlgOp;
                if (dlgOp.ShowDialog() == DialogResult.OK)
                {

                }
            }

        }

        private void ClickListaProductos()
        {

            if (this.DlgProductos.GrdLista.CurrentCell.ColumnIndex == 7)
            {
                int row = (int)this.DlgProductos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];

                var dlgOp = new DlgOperaciones(cuenta, this.Cliente);
                this.DlgOpen = dlgOp;
                if (dlgOp.ShowDialog() == DialogResult.OK)
                {

                }
            }
            else if (this.DlgProductos.GrdLista.CurrentCell.ColumnIndex == 8)
            {
                int row = (int)this.DlgProductos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];
                var transf = this.Search.BuscarTransferenciasCuenta(cuenta);
                var dlgTransf = new DlgTransferencias(transf, this.Cliente);
                this.DlgOpen = dlgTransf;
                if (dlgTransf.ShowDialog() == DialogResult.OK)
                {

                }
            }

        }

        void BuildSearch()
        {
            var dlgSearch = new DlgSearch();
            this.DlgOpen = dlgSearch;
            //Si se eligió búsqueda por persona
            if (dlgSearch.ShowDialog() == DialogResult.OK)
            {
                var op = dlgSearch.cbType.SelectedIndex;

                switch (op)
                {
                    case -1:
                        {
                            MessageBox.Show("Seleccione un tipo de búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dlgSearch.DialogResult = DialogResult.Cancel;
                            dlgSearch.Close();
                        }
                        break;
                    case 0:
                        {
                            if (this.Clientes.List.Count > 0) //Si hay clientes en el banco
                            {
                                var dlgClient = new DlgSearchClient(this.Clientes); //crea el dialogo para Buscar por persona
                                if (dlgClient.ShowDialog() == DialogResult.OK)
                                {
                                    if ((dlgClient.cbType.SelectedIndex == -1) || (dlgClient.cbClient.SelectedIndex == -1)) //si no se eligió ninguna opción de busqueda
                                    {
                                        MessageBox.Show("No se puede dejar ningún campo en blanco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        dlgClient.DialogResult = DialogResult.Cancel;
                                        dlgClient.Close();
                                    }
                                    else //si se eligieron todas las opciones de busqueda
                                    {
                                        this.SearchClient(dlgClient);
                                    }
                                }
                            }
                            else//si no hay clientes
                            {
                                MessageBox.Show("No existen clientes en el banco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        break;
                    //Si se eligió búsqueda en todo el banco

                    case 1:
                        {
                            if (this.Transferencias.List.Count > 0)//Si hay transferencias en el banco
                            {
                                var listTransf = this.Search.BuscarTransferenciasBanco();
                                var dlgTransf = new DlgTransferencias(listTransf, null);
                                //var listTransf = this.Search.BuscarTransferenciasBanco();
                                if (dlgTransf.ShowDialog() == DialogResult.OK)
                                {

                                }
                            }
                            else
                            {
                                MessageBox.Show("No existen trasferencias en el banco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        break;
                }
            }
        }

        void SearchClient(DlgSearchClient dlg)
        {
            var op = dlg.cbType.Text;
            var dni = dlg.cbClient.Text.Split('-')[1].ToString();
            this.Cliente = Search.GetClient(dni);
            switch (op)
            {
                case "Productos":
                    {
                        var listCuentas = this.Search.BuscarCuentas(dni);
                        var dlgProd = new DlgProductos(listCuentas, this.Cliente);
                        this.DlgProductos = dlgProd;
                        this.DlgProductos.GrdLista.Click += (sender, e) => this.ClickListaProductos();

                        if (dlgProd.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }
                    break;
                case "Movimientos":
                    {
                        var listCuentas = this.Search.BuscarCuentas(dni);
                        var dlgMovs = new DlgMovimientos(listCuentas, this.Cliente);
                        this.DlgMovimientos = dlgMovs;
                        this.DlgMovimientos.GrdLista.Click += (sender, e) => this.ClickListaMovimientos();

                        if (dlgMovs.ShowDialog() == DialogResult.OK)
                        {


                        }
                    }
                    break;
                case "Transferencias":
                    {
                        var listTransf = this.Search.BuscarTransferencias(dni);
                        var dlgTransf = new DlgTransferencias(listTransf, this.Cliente);
                        this.DlgOpen = dlgTransf;
                        if (dlgTransf.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }
                    break;
            }
        }

        void OnQuit()
        {
            Application.Exit();
        }

        void Salir()
        {
            //   this.Registro.GuardaXml();
            Application.Exit();
        }
        public MainWindowView MainWindowView
        {
            get; private set;
        }
        private Dlg DlgOpen;
        private RegistroCuentas Cuentas { get; set; }
        private RegistroClientes Clientes { get; set; }
        private RegistroTransferencias Transferencias { get; set; }
        private Busqueda Search { get; set; }
        private Cliente Cliente;
    }
}
