﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Busquedas.GUI.Dlgs
{
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;
    public  class DlgTransferencias : Dlg
    {

        public DlgTransferencias(List<Transferencia> t, Cliente cl)
        {
            this.Transferencias = t;
            this.Cliente = cl;
            this.Build();
        }

        private void Build()
        {
            this.BuildStatus();

            this.SuspendLayout();

            this.pnlPpal = new TableLayoutPanel { Dock = DockStyle.Fill };

            this.pnlPpal.SuspendLayout();
            if(this.Cliente != null) { 
                this.pnlPpal.Controls.Add(BuildInformation());
            }
            this.pnlPpal.Controls.Add(this.BuildPanelLista());
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(1000, 500);
            this.Resize += (obj, e) => this.ResizeWindow();
            this.Text = "Transferencias";

            this.ResumeLayout(true);
            this.Actualiza();
            this.ResizeWindow();
        }
        public Panel BuildInformation()
        {
            this.pnlInfo = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(1000, 50)
            };
            var lbName = new Label
            {
                Text = "Nombre: " + this.Cliente.Nombre,
                Dock = DockStyle.Top
            };
            var lbDni = new Label
            {
                Text = "Dni: " + this.Cliente.DNI,
                Dock = DockStyle.Bottom
            };

            this.pnlInfo.Controls.Add(lbName);
            this.pnlInfo.Controls.Add(lbDni);

            return pnlInfo;
        }

        public Panel BuildPanelLista()
        {
            this.pnlLista = new Panel();
            this.pnlLista.SuspendLayout();
            this.pnlLista.Dock = DockStyle.Fill;


            // Crear gridview
            this.GrdLista = new DataGridView();
            this.GrdLista.Dock = DockStyle.Fill;
          //  this.GrdLista.Height = this.Transferencias.Count;
            this.GrdLista.AllowUserToResizeRows = false;
            this.GrdLista.RowHeadersVisible = false;
            this.GrdLista.AutoGenerateColumns = false;
            this.GrdLista.MultiSelect = false;
            this.GrdLista.AllowUserToAddRows = false;
            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate1 = new DataGridViewTextBoxCell();
            var textCellTemplate2 = new DataGridViewTextBoxCell();
            var textCellTemplate3 = new DataGridViewTextBoxCell();
            var textCellTemplate4 = new DataGridViewTextBoxCell();

            textCellTemplate0.Style.BackColor = Color.LightGray;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.BackColor = Color.Wheat;
            textCellTemplate1.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            textCellTemplate2.Style.BackColor = Color.Wheat;
            textCellTemplate2.Style.ForeColor = Color.Black;
            textCellTemplate3.Style.BackColor = Color.Wheat;
            textCellTemplate3.Style.ForeColor = Color.Black;
            textCellTemplate4.Style.BackColor = Color.Wheat;
            textCellTemplate4.Style.ForeColor = Color.Black;

            var column0 = new DataGridViewTextBoxColumn();
            var column1 = new DataGridViewTextBoxColumn();
            var column2 = new DataGridViewTextBoxColumn();
            var column3 = new DataGridViewTextBoxColumn();
            var column4 = new DataGridViewTextBoxColumn();

            column0.SortMode = DataGridViewColumnSortMode.NotSortable;
            column1.SortMode = DataGridViewColumnSortMode.NotSortable;
            column2.SortMode = DataGridViewColumnSortMode.NotSortable;
            column3.SortMode = DataGridViewColumnSortMode.NotSortable;
            column4.SortMode = DataGridViewColumnSortMode.NotSortable;

            column0.CellTemplate = textCellTemplate0;
            column1.CellTemplate = textCellTemplate1;
            column2.CellTemplate = textCellTemplate2;
            column3.CellTemplate = textCellTemplate3;
            column4.CellTemplate = textCellTemplate4;

            column0.HeaderText = "#";
            column0.ReadOnly = true;
            column0.Width = 5;
            column1.HeaderText = "Tipo";
            column1.ReadOnly = true;
            column1.Width = 15;
            column2.HeaderText = "ccc Origen";
            column2.Width = 20;
            column2.ReadOnly = true;
            column3.HeaderText = "ccc Destino";
            column3.Width = 20;
            column3.ReadOnly = true;
            column4.HeaderText = "Importe";
            column4.Width = 15;
            column4.ReadOnly = true;


            this.GrdLista.Columns.AddRange(new DataGridViewColumn[] {
                column0, column1, column2, column3,column4
            });


            this.pnlLista.Controls.Add(this.GrdLista);
            this.pnlLista.ResumeLayout(false);

            return this.pnlLista;
        }

        public void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.pnlPpal.ClientRectangle.Width;

            // Redimensionar la tabla
            this.GrdLista.Width = width;

            this.GrdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .30);
            this.GrdLista.Columns[3].Width =
                                (int)System.Math.Floor(width * .30);
            this.GrdLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .20);

        }

        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }
        private void BuildStatus()
        {
            this.SbStatus = new StatusBar();
            this.SbStatus.Dock = DockStyle.Bottom;
            this.Controls.Add(this.SbStatus);
        }
        public void Actualiza()
        {
            int numElements = this.Transferencias.Count;
            this.SbStatus.Text = ("Numero transferencias : " + numElements);
            for (int i = 0; i < numElements; i++)
            {
                if (this.GrdLista.Rows.Count <= i)
                {
                    this.GrdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.GrdLista.Rows.Count - numElements;
            for (; numExtra > 0; --numExtra)
            {
                this.GrdLista.Rows.RemoveAt(numElements);
            }
        }

        private void ActualizarFilaLista(int numRow)
        {
            if (numRow < 0
              || numRow > this.GrdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numRow));
            }

            DataGridViewRow fila = this.GrdLista.Rows[numRow];
            Transferencia transfer = this.Transferencias[numRow];
            fila.Cells[0].Value = (numRow + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = transfer.Tipo;
            fila.Cells[2].Value = transfer.CCCorigen.CCC;
            fila.Cells[3].Value = transfer.CCCdestino.CCC;
            fila.Cells[4].Value = transfer.Importe;

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = transfer.ToString();
            }
        }
        private List<Transferencia> Transferencias
        {
            get; set;
        }
        private readonly Cliente Cliente;
        private Panel pnlInfo;
        private Panel pnlLista;
        public TableLayoutPanel pnlPpal;
        public StatusBar SbStatus;
        public DataGridView GrdLista;

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DlgTransferencias
            // 
            this.ClientSize = new System.Drawing.Size(780, 424);
            this.Name = "DlgTransferencias";
            this.ResumeLayout(false);

        }


    }
}
