﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace Busquedas.GUI.Dlgs
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;
    using System.Text;
    using System.Windows.Forms.DataVisualization.Charting;

    public class DlgCuentas : Dlg
    {

        public Series trans3 = new Series();
        public static double[] Trans;
        public static double[] Dep;
        public static double[] Ret;




        public DlgCuentas(List<Cuenta> c, Cliente cl)
        {
            this.InitializeComponentResumenSaldos();
            this.Cuentas = c;
            this.Cliente = cl;
            this.Build();
            this.Font = new System.Drawing.Font("Century Gothic", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        }

        private void Build()
        {
            //this.BuildStatus();
            this.SuspendLayout();
            Panel ListPanel = this.BuildPanelLista();

            this.SuspendLayout();

            this.pnlPpal = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill
            };

            this.pnlPpal.SuspendLayout();
            if (this.Cliente != null)
            {
                this.pnlPpal.Controls.Add(BuildInformation());
            }
             pnlPpal.Size = new Size(880, 200);
            
           



            this.Controls.Add(this.pnlPpal);
            //this.pnlPpal.Controls.Add(ListPanel);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(500, 600);
            this.Text = "Cuentas";
            this.Resize += (obj, e) => this.ResizeWindow();
            //this.Location = new System.Drawing.Point(45, 170);





            this.pnlPpal.Controls.Add(this.BuildPanelLista());
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.ResumeLayout(false);

            

            this.ResumeLayout(true);
            this.Actualiza();

            this.ResizeWindow();

   


        }

        public Panel BuildInformation()
        {
            this.pnlInfo = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(50, 50)
            };
            var lbName = new Label
            {
                Text = "Nombre: " + this.Cliente.Nombre,
                Dock = DockStyle.Top
            };
            var lbDni = new Label
            {
                Text = "Dni: " + this.Cliente.DNI,
                Dock = DockStyle.Top
            };

            this.pnlInfo.Controls.Add(lbName);
            this.pnlInfo.Controls.Add(lbDni);

            return this.pnlInfo;
        }

        public Panel BuildPanelLista()
        {
            this.pnlLista = new Panel();
            this.pnlLista.SuspendLayout();
            this.pnlLista.Dock = DockStyle.Fill;
            this.GrdLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };
            // Crear gridview
           
            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate1 = new DataGridViewTextBoxCell();
            var textCellTemplate2 = new DataGridViewTextBoxCell();
            var textCellTemplate3 = new DataGridViewTextBoxCell();
            var textCellTemplate4 = new DataGridViewTextBoxCell();
            var textCellTemplate5 = new DataGridViewTextBoxCell();
            var textCellTemplate6 = new DataGridViewTextBoxCell();

            this.GrdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.GrdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.PeachPuff;

            

            textCellTemplate0.Style.BackColor = Color.LightGray;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.BackColor = Color.Wheat;
            textCellTemplate1.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.TopCenter;
            textCellTemplate2.Style.BackColor = Color.Wheat;
            textCellTemplate2.Style.ForeColor = Color.Black;
            textCellTemplate3.Style.BackColor = Color.Wheat;
            textCellTemplate3.Style.ForeColor = Color.Black;
            textCellTemplate4.Style.BackColor = Color.Wheat;
            textCellTemplate4.Style.ForeColor = Color.Black;
            textCellTemplate5.Style.BackColor = Color.Wheat;
            textCellTemplate5.Style.ForeColor = Color.Black;
            textCellTemplate6.Style.BackColor = Color.Wheat;
            textCellTemplate6.Style.ForeColor = Color.Black;

            var column0 = new DataGridViewTextBoxColumn();
            var column1 = new DataGridViewTextBoxColumn();
            var column2 = new DataGridViewTextBoxColumn();
            var column3 = new DataGridViewTextBoxColumn();
            var column4 = new DataGridViewTextBoxColumn();
            var column5 = new DataGridViewTextBoxColumn();
            var column6 = new DataGridViewTextBoxColumn();
            var gridCeldaButton = new DataGridViewButtonCell();

            column0.SortMode = DataGridViewColumnSortMode.NotSortable;
            column1.SortMode = DataGridViewColumnSortMode.NotSortable;
            column2.SortMode = DataGridViewColumnSortMode.NotSortable;
            column3.SortMode = DataGridViewColumnSortMode.NotSortable;
            column4.SortMode = DataGridViewColumnSortMode.NotSortable;
            column5.SortMode = DataGridViewColumnSortMode.NotSortable;
            column6.SortMode = DataGridViewColumnSortMode.NotSortable;

            column0.CellTemplate = textCellTemplate0;
            column1.CellTemplate = textCellTemplate1;
            column2.CellTemplate = textCellTemplate2;
            column3.CellTemplate = textCellTemplate3;
            column4.CellTemplate = textCellTemplate4;
            column5.CellTemplate = textCellTemplate5;
            column6.CellTemplate = textCellTemplate6;

            column0.HeaderText = "#";
            column0.ReadOnly = true;
            column0.Width = 5;
            column1.HeaderText = "CCC";
            column1.ReadOnly = true;
            column1.Width = 20;
            column2.HeaderText = "Tipo";
            column2.Width = 20;
            column2.ReadOnly = true;
            column3.HeaderText = "Saldo";
            column3.Width = 30;
            column3.ReadOnly = true;
            column4.HeaderText = "Titulares";
            column4.Width = 20;
            column4.ReadOnly = true;
            column5.HeaderText = "Fecha Apertura";
            column5.Width = 30;
            column5.ReadOnly = true;
            column6.HeaderText = "Intereses Mensuales";
            column6.Width = 30;
            column6.ReadOnly = true;

            var c7 = new DataGridViewButtonColumn
            {
                
                CellTemplate = gridCeldaButton,
                HeaderText = "Eliminar",
                Width = 100,
                ReadOnly = true

            };

            var c8 = new DataGridViewButtonColumn
            {
               
                CellTemplate = gridCeldaButton,
                HeaderText = "Modificar",
                Width = 100,
                ReadOnly = true

            };

            var c9 = new DataGridViewButtonColumn
            {
                
                CellTemplate = gridCeldaButton,
                HeaderText = "Transferencias",
                Width = 100,
                ReadOnly = true

            };

            var c10 = new DataGridViewButtonColumn
            {
                
                CellTemplate = gridCeldaButton,
                HeaderText = "Movimientos",
                Width = 100,
                ReadOnly = true

            };
            
            this.GrdLista.Columns.AddRange(new DataGridViewColumn[] {
                column0, column1, column2, column3,column4,column5,column6,c7,c8,c9,c10
            });

            
            this.pnlLista.Controls.Add(this.GrdLista);
            this.pnlLista.ResumeLayout(false);

            return this.pnlLista;
        }

        public void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.pnlPpal.ClientRectangle.Width;

            // Redimensionar la tabla
            this.GrdLista.Width = width;

            this.GrdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .15);
            this.GrdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[3].Width =
                               (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .20);
            this.GrdLista.Columns[5].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[6].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[7].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[8].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[9].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[10].Width =
                                (int)System.Math.Floor(width * .05);
        }

        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }
        private void BuildStatus()
        {
            this.SbStatus = new StatusBar();
            this.SbStatus.Dock = DockStyle.Bottom;
            this.Controls.Add(this.SbStatus);
            


        }
        public void Actualiza()
        {
            int numElements = this.Cuentas.Count;
            //this.SbStatus.Text = ("Numero Cuentas : " + numElements);
            for (int i = 0; i < numElements; i++)
            {
                if (this.GrdLista.Rows.Count <= i)
                {
                    this.GrdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.GrdLista.Rows.Count - numElements;
            for (; numExtra > 0; --numExtra)
            {
                this.GrdLista.Rows.RemoveAt(numElements);
            }
        }

        private void ActualizarFilaLista(int numRow)
        {
            if (numRow < 0
              || numRow > this.GrdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numRow));
            }

            DataGridViewRow fila = this.GrdLista.Rows[numRow];
            Cuenta cuenta = this.Cuentas[numRow];

            fila.Cells[0].Value = (numRow + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = cuenta.CCC;
            fila.Cells[2].Value = cuenta.Tipo;
            fila.Cells[3].Value = cuenta.Saldo;
            fila.Cells[4].Value = GetTitulares(cuenta);
            fila.Cells[5].Value = cuenta.FechaApertura.ToString();
            fila.Cells[6].Value = cuenta.InteresMensual.ToString();
            fila.Cells[7].Value = "o";
            fila.Cells[8].Value = "o";
            fila.Cells[9].Value = "o";
            fila.Cells[10].Value = "o";


            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = cuenta.ToString();
            }
        }

        private string GetTitulares(Cuenta cuenta)
        {
            List<Cliente> titulares = cuenta.Titulares;
            StringBuilder toret = new StringBuilder();
            foreach (Cliente c in titulares)
            {
                toret.Append(c.Nombre).Append(", ");
            }
            return toret.ToString();

        }

        

        


        public List<Cuenta> Cuentas
        {
            get; set;
        }
        private Panel pnlInfo;
        public readonly Cliente Cliente;
        private Panel pnlLista;
        public Panel pnlPpal;
        public StatusBar SbStatus;
        public DataGridView GrdLista;
        private Chart chart1;

       
        private void InitializeComponentResumenSaldos()
        {
            ChartArea chartArea1 = new ChartArea();
            Legend legend1 = new Legend();


            Series depos4 = new Series();
            Series retir4 = new Series();
            Series trans4 = new Series();


            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.PeachPuff;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(45, 240);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;

            depos4.ChartArea = "ChartArea1";
            depos4.Legend = "Legend1";
            depos4.Name = "Depósitos";
            depos4.Points.AddY(0);

            retir4.ChartArea = "ChartArea1";
            retir4.Legend = "Legend1";
            retir4.Name = "Retiradas";

            foreach (double importe in Trans)
            {
                trans4.Points.AddY(importe);
            }

            foreach (double importe in Dep)
            {
                depos4.Points.AddY(importe);
            }
            foreach (double importe in Ret)
            {
                retir4.Points.AddY(importe);
            }



            trans4.ChartArea = "ChartArea1";
            trans4.Legend = "Legend1";
            trans4.Name = "Tranferencias";



            this.chart1.Series.Add(depos4);
            this.chart1.Series.Add(retir4);
            this.chart1.Series.Add(trans4);
            this.chart1.Size = new System.Drawing.Size(810, 300);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // ResumenSaldos
            // 
            this.ClientSize = new System.Drawing.Size(881, 364);
            this.Controls.Add(this.chart1);
            this.Name = "ResumenSaldos";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
