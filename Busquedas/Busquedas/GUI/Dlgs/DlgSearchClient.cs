﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Busquedas.GUI.Dlgs
{
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;

   public class DlgSearchClient : Dlg
    {
        public DlgSearchClient(RegistroClientes clientes)
        {
            this.Clientes = clientes;
            this.Build();
        }

        private void Build()
        {
            this.SuspendLayout();

            var pnlInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            pnlInserta.SuspendLayout();
            this.Controls.Add(pnlInserta);
            var pnlType = this.BuildTypePanel();
            pnlInserta.Controls.Add(pnlType);
            var pnlClient = this.BuildClientPanel();
            pnlInserta.Controls.Add(pnlClient);
            var pnlBotones = this.BuildBotonesPanel();
            pnlInserta.Controls.Add(pnlBotones);

            pnlInserta.ResumeLayout(true);

            this.Text = "Búsqueda por Persona";
            this.Size = new Size(450,
                            pnlType.Height + pnlClient.Height + pnlBotones.Height);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        private Panel BuildTypePanel()
        {
            var pnlType = new Panel { Dock = DockStyle.Top };
            this.cbType = new ComboBox { Dock = DockStyle.Fill };
            var lbType = new Label
            {
                Text = "Tipo Búsqueda: ",
                Dock = DockStyle.Left
            };

            cbType.Anchor = AnchorStyles.Bottom;
            cbType.DropDownStyle = ComboBoxStyle.DropDownList;
            cbType.DropDownWidth = 280;
            cbType.Items.AddRange(new string[] { "Productos", "Movimientos", "Transferencias" });

            pnlType.Controls.Add(cbType);
            pnlType.Controls.Add(lbType);

            return pnlType;
        }

        private Panel BuildClientPanel()
        {

            var pnlClient = new Panel { Dock = DockStyle.Top };
            this.cbClient = new ComboBox { Dock = DockStyle.Fill };
            var lbClient = new Label
            {
                Text = "Elige Persona: ",
                Dock = DockStyle.Left
            };
            foreach (Cliente c in Clientes.List)
            {
                this.cbClient.Items.Add(c.Nombre + "-" + c.DNI);
            }
            cbClient.Anchor = AnchorStyles.Bottom;
            cbClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbClient.DropDownWidth = 280;

            pnlClient.Controls.Add(cbClient);
            pnlClient.Controls.Add(lbClient);

            return pnlClient;
            
        }
        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }

        public ComboBox cbType;
        public ComboBox cbClient;
        public string Type => this.cbType.Text;
        public string Client => this.cbClient.Text;

        private RegistroClientes Clientes { get; set; }

    }
}
