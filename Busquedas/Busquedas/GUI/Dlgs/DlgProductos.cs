﻿using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace Busquedas.GUI.Dlgs
{
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;
    using System.Text;

    public class DlgProductos : Dlg
    {

        public DlgProductos(List<Cuenta> c, Cliente cl)
        {
            this.Cuentas = c;
            this.Cliente = cl;
            this.Build();
        }

        private Cuenta Cuenta
        {
            get; set;
        }

        private void Build()
        {
            this.SuspendLayout();
            this.BuildStatus();

            this.pnlPpal = new TableLayoutPanel { Dock = DockStyle.Fill };
            this.pnlPpal.SuspendLayout();
            if (this.Cliente != null)
            {
                this.pnlPpal.Controls.Add(BuildInformation());
            }
            this.pnlPpal.Controls.Add(this.BuildPanelLista());
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(1200, 500);
            this.Resize += (obj, e) => this.ResizeWindow();
            this.Text = "Cuentas";

            this.ResumeLayout(true);
            this.Actualiza();

            this.ResizeWindow();
        }

        public Panel BuildInformation()
        {
            this.pnlInfo = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(1000, 50)
            };
            var lbName = new Label
            {
                Text = "Nombre: " + this.Cliente.Nombre,
                Dock = DockStyle.Top
            };
            var lbDni = new Label
            {
                Text = "Dni: " + this.Cliente.DNI,
                Dock = DockStyle.Bottom
            };

            this.pnlInfo.Controls.Add(lbName);
            this.pnlInfo.Controls.Add(lbDni);

            return this.pnlInfo;
        }
        public Panel BuildPanelLista()
        {
            this.pnlLista = new Panel();
            this.pnlLista.SuspendLayout();
            this.pnlLista.Dock = DockStyle.Fill;

            // Crear gridview
            this.GrdLista = new DataGridView();
            this.GrdLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };
            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate1 = new DataGridViewTextBoxCell();
            var textCellTemplate2 = new DataGridViewTextBoxCell();
            var textCellTemplate3 = new DataGridViewTextBoxCell();
            var textCellTemplate4 = new DataGridViewTextBoxCell();
            var textCellTemplate5 = new DataGridViewTextBoxCell();
            var textCellTemplate6 = new DataGridViewTextBoxCell();
            var textCellTemplate7 = new DataGridViewButtonCell();
            var textCellTemplate8 = new DataGridViewButtonCell();

            textCellTemplate0.Style.BackColor = Color.LightGray;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.BackColor = Color.Wheat;
            textCellTemplate1.Style.ForeColor = Color.Black;
            textCellTemplate2.Style.BackColor = Color.Wheat;
            textCellTemplate2.Style.ForeColor = Color.Black;
            textCellTemplate3.Style.BackColor = Color.Wheat;
            textCellTemplate3.Style.ForeColor = Color.Black;
            textCellTemplate4.Style.BackColor = Color.Wheat;
            textCellTemplate4.Style.ForeColor = Color.Black;
            textCellTemplate5.Style.BackColor = Color.Wheat;
            textCellTemplate5.Style.ForeColor = Color.Black;
            textCellTemplate6.Style.BackColor = Color.Wheat;
            textCellTemplate6.Style.ForeColor = Color.Black;
            textCellTemplate7.Style.BackColor = Color.Wheat;
            textCellTemplate7.Style.ForeColor = Color.Black;
            textCellTemplate8.Style.BackColor = Color.Wheat;
            textCellTemplate8.Style.ForeColor = Color.Black;

            var column0 = new DataGridViewTextBoxColumn();
            var column1 = new DataGridViewTextBoxColumn();
            var column2 = new DataGridViewTextBoxColumn();
            var column3 = new DataGridViewTextBoxColumn();
            var column4 = new DataGridViewTextBoxColumn();
            var column5 = new DataGridViewTextBoxColumn();
            var column6 = new DataGridViewTextBoxColumn();
            var column7 = new DataGridViewButtonColumn();
            var column8 = new DataGridViewButtonColumn();

            column0.SortMode = DataGridViewColumnSortMode.NotSortable;
            column1.SortMode = DataGridViewColumnSortMode.NotSortable;
            column2.SortMode = DataGridViewColumnSortMode.NotSortable;
            column3.SortMode = DataGridViewColumnSortMode.NotSortable;
            column4.SortMode = DataGridViewColumnSortMode.NotSortable;
            column5.SortMode = DataGridViewColumnSortMode.NotSortable;
            column6.SortMode = DataGridViewColumnSortMode.NotSortable;
            column7.SortMode = DataGridViewColumnSortMode.NotSortable;
            column8.SortMode = DataGridViewColumnSortMode.NotSortable;

            column0.CellTemplate = textCellTemplate0;
            column1.CellTemplate = textCellTemplate1;
            column2.CellTemplate = textCellTemplate2;
            column3.CellTemplate = textCellTemplate3;
            column4.CellTemplate = textCellTemplate4;
            column5.CellTemplate = textCellTemplate5;
            column6.CellTemplate = textCellTemplate6;
            column7.CellTemplate = textCellTemplate7;
            column8.CellTemplate = textCellTemplate8;

            column0.HeaderText = "#";
            column0.ReadOnly = true;
            column0.Width = 5;
            column1.HeaderText = "CCC";
            column1.ReadOnly = true;
            column1.Width = 20;
            column2.HeaderText = "Tipo";
            column2.Width = 20;
            column2.ReadOnly = true;
            column3.HeaderText = "Saldo";
            column3.Width = 30;
            column3.ReadOnly = true;
            column4.HeaderText = "Titulares";
            column4.Width = 20;
            column4.ReadOnly = true;
            column5.HeaderText = "Fecha Apertura";
            column5.Width = 30;
            column5.ReadOnly = true;
            column6.HeaderText = "Intereses Mensuales";
            column6.Width = 30;
            column6.ReadOnly = true;
            column7.HeaderText = "Movimientos";
            column7.Width = 20;
            column7.ReadOnly = true;
            column8.HeaderText = "Transferencias";
            column8.Width = 20;
            column8.ReadOnly = true;

            this.GrdLista.Columns.AddRange(new DataGridViewColumn[] {
                column0, column1, column2, column3,column4,column5,column6,column7, column8
            });


            this.pnlLista.Controls.Add(this.GrdLista);
            this.pnlLista.ResumeLayout(false);

            return this.pnlLista;
        }

        public void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.pnlPpal.ClientRectangle.Width;

            // Redimensionar la tabla
            this.GrdLista.Width = width;

            this.GrdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .15);
            this.GrdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .05);
            this.GrdLista.Columns[3].Width =
                               (int)System.Math.Floor(width * .15);
            this.GrdLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .15);
            this.GrdLista.Columns[5].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[6].Width =
                                (int)System.Math.Floor(width * .15);
            this.GrdLista.Columns[7].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[8].Width =
                                (int)System.Math.Floor(width * .10);

        }

        private Panel BuildLabelCuenta(Cuenta c)
        {
            this.pnlCCC = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(1000, 50)
            };
            var lbccc = new Label
            {
                Text = "CCC: " + c.CCC,
                Dock = DockStyle.Top
            };

            this.pnlCCC.Controls.Add(lbccc);

            return this.pnlCCC;
        }


        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }

        private void BuildStatus()
        {
            this.SbStatus = new StatusBar();
            this.SbStatus.Dock = DockStyle.Bottom;
            this.Controls.Add(this.SbStatus);
        }

        public void Actualiza()
        {
            int numElements = this.Cuentas.Count;
            this.SbStatus.Text = ("Numero Cuentas : " + numElements);
            for (int i = 0; i < numElements; i++)
            {
                if (this.GrdLista.Rows.Count <= i)
                {
                    this.GrdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.GrdLista.Rows.Count - numElements;
            for (; numExtra > 0; --numExtra)
            {
                this.GrdLista.Rows.RemoveAt(numElements);
            }
        }

        private void ActualizarFilaLista(int numRow)
        {
            if (numRow < 0
              || numRow > this.GrdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numRow));
            }

            DataGridViewRow fila = this.GrdLista.Rows[numRow];
            Cuenta cuenta = this.Cuentas[numRow];

            fila.Cells[0].Value = (numRow + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = cuenta.CCC;
            fila.Cells[2].Value = cuenta.Tipo;
            fila.Cells[3].Value = cuenta.Saldo;
            fila.Cells[4].Value = GetTitulares(cuenta);
            fila.Cells[5].Value = cuenta.FechaApertura.ToString();
            fila.Cells[6].Value = cuenta.InteresMensual.ToString();
            fila.Cells[7].Value = "Ver Movimientos";
            fila.Cells[8].Value = "Ver Transferencias";


            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = cuenta.ToString();
            }
        }

        private string GetTitulares(Cuenta cuenta)
        {
            List<Cliente> titulares = cuenta.Titulares;
            StringBuilder toret = new StringBuilder();
            foreach (Cliente c in titulares)
            {
                toret.Append(c.Nombre).Append(", ");
            }
            return toret.ToString();

        }
        public List<Cuenta> Cuentas
        {
            get; set;
        }
        private Panel pnlInfo;
        private readonly Cliente Cliente;
        private Panel pnlLista;
        public Panel pnlPpal;
        private Panel pnlCCC;
        public DataGridView GrdLista;
        public StatusBar SbStatus;
        public MainMenu mPpal = new MainMenu();


    }
}
