﻿using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace Busquedas.GUI.Dlgs
{
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;
    using System.Text;
    using System.Windows.Forms.DataVisualization.Charting;

    public class DlgOperaciones : Dlg
    {
        public static double[] Trans;
        public static double[] Dep;
        public static double[] Ret;


        public DlgOperaciones(Cuenta c, Cliente cl)
        {
            this.InitializeComponentResumenCuenta();
            this.Cuenta = c;
            this.Cliente = cl;
            this.Build();
            this.Font = new System.Drawing.Font("Century Gothic", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        }

        private Cuenta Cuenta
        {
            get; set;
        }

        private List<Operacion> Operaciones
        {
            get
            {
                List<Operacion> op = new List<Operacion>();
                foreach (Operacion o in this.Cuenta.Depositos)
                {
                    op.Add(o);
                }
                foreach (Operacion o in this.Cuenta.Retiradas)
                {
                    op.Add(o);
                }
                return op;
            }
        }

        private void Build()
        {
            this.SuspendLayout();
            Panel ListPanel = this.BuildPanelLista();

            this.SuspendLayout();

            this.pnlPpal = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill
            };

            this.pnlPpal.SuspendLayout();
            if (this.Cliente != null)
            {
                this.pnlPpal.Controls.Add(BuildInformation());
            }
            pnlPpal.Size = new Size(880, 200);





            this.Controls.Add(this.pnlPpal);
            //this.pnlPpal.Controls.Add(ListPanel);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(500, 600);
            this.Text = "Operaciones";
            this.Resize += (obj, e) => this.ResizeWindow();
            //this.Location = new System.Drawing.Point(45, 170);





            this.pnlPpal.Controls.Add(this.BuildPanelLista());
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.ResumeLayout(false);



            this.ResumeLayout(true);
            this.Actualiza();

            this.ResizeWindow();

        }
        public Panel BuildInformation()
        {
            this.pnlInfo = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(50, 50)
            };
            var lbName = new Label
            {
                Text = "Nombre: " + this.Cliente.Nombre,
                Dock = DockStyle.Top
            };
            var lbDni = new Label
            {
                Text = "Dni: " + this.Cliente.DNI,
                Dock = DockStyle.Top
            };

            this.pnlInfo.Controls.Add(lbName);
            this.pnlInfo.Controls.Add(lbDni);

            return this.pnlInfo;
        }

        private Panel BuildPanelLista()
        {

            this.pnlLista = new Panel();
            this.pnlLista.SuspendLayout();
            this.pnlLista.Dock = DockStyle.Fill;

            // Crear gridview
            this.GrdLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };

            this.GrdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.GrdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.PeachPuff;

            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate2 = new DataGridViewTextBoxCell();
            var textCellTemplate3 = new DataGridViewTextBoxCell();
            var textCellTemplate4 = new DataGridViewTextBoxCell();

            textCellTemplate0.Style.BackColor = Color.LightGray;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate2.Style.BackColor = Color.Wheat;
            textCellTemplate2.Style.ForeColor = Color.Black;
            textCellTemplate3.Style.BackColor = Color.Wheat;
            textCellTemplate3.Style.ForeColor = Color.Black;
            textCellTemplate4.Style.BackColor = Color.Wheat;
            textCellTemplate4.Style.ForeColor = Color.Black;

            var column0 = new DataGridViewTextBoxColumn();
            var column2 = new DataGridViewTextBoxColumn();
            var column3 = new DataGridViewTextBoxColumn();
            var column4 = new DataGridViewTextBoxColumn();

            column0.SortMode = DataGridViewColumnSortMode.NotSortable;
            column2.SortMode = DataGridViewColumnSortMode.NotSortable;
            column3.SortMode = DataGridViewColumnSortMode.NotSortable;
            column4.SortMode = DataGridViewColumnSortMode.NotSortable;

            column0.CellTemplate = textCellTemplate0;
            column2.CellTemplate = textCellTemplate2;
            column3.CellTemplate = textCellTemplate3;
            column4.CellTemplate = textCellTemplate4;

            column0.HeaderText = "#";
            column0.ReadOnly = true;
            column0.Width = 10;
            column2.HeaderText = "Tipo";
            column2.Width = 20;
            column2.ReadOnly = true;
            column3.HeaderText = "Importe";
            column3.Width = 30;
            column3.ReadOnly = true;
            column4.HeaderText = "Fecha";
            column4.Width = 20;
            column4.ReadOnly = true;

            this.GrdLista.Columns.AddRange(new DataGridViewColumn[] {
                column0, column2, column3,column4
            });


            this.pnlLista.Controls.Add(this.GrdLista);
            this.pnlLista.ResumeLayout(false);

            return this.pnlLista;
        }

        private Panel BuildLabelCuenta()
        {
            this.pnlCCC = new Panel
            {
                Dock = DockStyle.Top,
                Size = new Size(1000, 50)
            };
            var lbccc = new Label
            {
                Text = "CCC: " + this.Cuenta.CCC,
                Dock = DockStyle.Top
            };

            this.pnlCCC.Controls.Add(lbccc);

            return this.pnlCCC;

        }

        public void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.ClientRectangle.Width;

            // Redimensionar la tabla
            this.GrdLista.Width = width;

            this.GrdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .10);
            this.GrdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .40);
            this.GrdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .25);
            this.GrdLista.Columns[3].Width =
                               (int)System.Math.Floor(width * .25);


        }

        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }

        public void Actualiza()
        {
            int numElements = this.Operaciones.Count;
            for (int i = 0; i < numElements; i++)
            {
                if (this.GrdLista.Rows.Count <= i)
                {
                    this.GrdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.GrdLista.Rows.Count - numElements;
            for (; numExtra > 0; --numExtra)
            {
                this.GrdLista.Rows.RemoveAt(numElements);
            }
        }


        private void ActualizarFilaLista(int numRow)
        {
            if (numRow < 0
              || numRow > this.GrdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numRow));
            }

            DataGridViewRow fila = this.GrdLista.Rows[numRow];
            Operacion op = this.Operaciones[numRow];

            fila.Cells[0].Value = (numRow + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = op.Tipo;
            fila.Cells[2].Value = op.Importe;
            fila.Cells[3].Value = op.Fecha.ToString();

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = op.ToString();
            }
        }

        private string GetTitulares(Cuenta cuenta)
        {
            List<Cliente> titulares = cuenta.Titulares;
            StringBuilder toret = new StringBuilder();
            foreach (Cliente c in titulares)
            {
                toret.Append(c.Nombre).Append(", ");
            }
            return toret.ToString();

        }
        private List<Cuenta> Cuentas
        {
            get; set;
        }
        private Panel pnlInfo;
        public readonly Cliente Cliente;
        private Panel pnlLista;
        public Panel pnlPpal;
        private Panel pnlCCC;
        public DataGridView GrdLista;

        private Chart chart1;

        private void InitializeComponentResumenCuenta()
        {
            ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();



            Series depos3 = new Series();
            Series retir3 = new Series();
            Series trans3 = new Series();


            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.PeachPuff;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(45, 250);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry;

            depos3.ChartArea = "ChartArea1";
            depos3.Legend = "Legend1";
            depos3.Name = "Depósitos";


            retir3.ChartArea = "ChartArea1";
            retir3.Legend = "Legend1";
            retir3.Name = "retiradas";

            foreach (double importe in Trans)
            {
                trans3.Points.AddY(importe);
            }

            foreach (double importe in Dep)
            {
                depos3.Points.AddY(importe);
            }
            foreach (double importe in Ret)
            {
                retir3.Points.AddY(importe);
            }


            trans3.ChartArea = "ChartArea1";
            trans3.Legend = "Legend1";
            trans3.Name = "Transferencias";


            this.chart1.Series.Add(depos3);
            this.chart1.Series.Add(retir3);
            this.chart1.Series.Add(trans3);
            this.chart1.Size = new System.Drawing.Size(793, 300);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // ResumenCuenta
            // 
            this.ClientSize = new System.Drawing.Size(895, 337);
            this.Controls.Add(this.chart1);
            this.Name = "ResumenCuenta";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }
    }

}
