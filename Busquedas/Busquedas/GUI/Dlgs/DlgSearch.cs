﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Busquedas.GUI.Dlgs
{
    using GestionClientes.Core;
    using GestionTransferencias.Core;
    using GestionCuentas;

    public class DlgSearch : Dlg
    {
        public DlgSearch()
        {
            this.Build();
        }
        private void Build()
        {
            this.SuspendLayout();

            var pnlSearch = new TableLayoutPanel { Dock = DockStyle.Fill };
            pnlSearch.SuspendLayout();
            this.Controls.Add(pnlSearch);
            var pnl = this.BuildSearch();
            pnlSearch.Controls.Add(pnl);
            var pnlBotones = this.BuildBotonesPanel();
            pnlSearch.Controls.Add(pnlBotones);

            pnlSearch.ResumeLayout(true);

            this.Text = "Búsqueda";
            this.Size = new Size(450,
                            pnl.Height + pnlBotones.Height);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        private Panel BuildSearch()
        {
            var pnlPerson = new Panel { Dock = DockStyle.Fill };
            this.cbType = new ComboBox();

            var lbType = new Label
            {
                Text = "Tipo Búsqueda: ",
                Dock = DockStyle.Left
            };

            cbType.Anchor = AnchorStyles.Bottom;
            cbType.DropDownStyle = ComboBoxStyle.DropDownList;
            cbType.DropDownWidth = 280;
            //cbType.Items.AddRange(new string[] { "Por Persona", "Todo el Banco"});
            cbType.Items.Add("Por Persona");
            cbType.Items.Add("Todo el banco");

            pnlPerson.Controls.Add(cbType);
            pnlPerson.Controls.Add(lbType);

            return pnlPerson;
        }
       
        private Panel BuildBotonesPanel()
        {
            var pnlBotones = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1,
                Dock = DockStyle.Fill
            };

            var btCierra = new Button();
            btCierra.DialogResult = DialogResult.Cancel;
            btCierra.Text = "&Cancelar";
            var btBusca = new Button();
            btBusca.DialogResult = DialogResult.OK;
            btBusca.Text = "&Buscar";
            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            this.AcceptButton = btBusca;
            this.CancelButton = btCierra;

            pnlBotones.Controls.Add(btBusca);
            pnlBotones.Controls.Add(btCierra);
            pnlBotones.Dock = DockStyle.Top;

            return pnlBotones;
        }

        public ComboBox cbType;
        public string Type => this.cbType.Text;

    }
}
