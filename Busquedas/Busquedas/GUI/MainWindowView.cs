﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using GestionClientes.View;

namespace Busquedas.GUI
{
    public class MainWindowView : Form
    {
        public MainWindowView()
        {
            this.BuildGUI();
            this.BuildMenu();
        }
        private void BuildGUI()
        {
            this.BuildMenu();

            this.SuspendLayout();
            this.pnlPpal = new Panel()
            {
                Dock = DockStyle.Fill
            };

            this.pnlPpal.SuspendLayout();
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(800, 500);
            this.Text = "Gestión Banco";

            this.ResumeLayout(true);
        }
        private void BuildMenu()
        {
            this.mPpal = new MainMenu();

            this.mArchivo = new MenuItem("&Archivo");
            this.mSearch = new MenuItem("&Buscar");

            this.opSalir = new MenuItem("&Salir");
            this.opSalir.Shortcut = Shortcut.CtrlQ;

            this.mArchivo.MenuItems.Add(this.opSalir);
            this.mArchivo.MenuItems.Add(this.mSearch);

            this.mPpal.MenuItems.Add(this.mArchivo);
            this.mPpal.MenuItems.Add(this.mSearch);
            this.Menu = mPpal;
        }

 
        private MainMenu mPpal;
        public MenuItem mArchivo;
        public MenuItem mSearch;
        public MenuItem opSalir;
        private Panel pnlPpal;

        public static implicit operator MainWindowView(GestionClientes.View.MainWindowView v)
        {
            throw new NotImplementedException();
        }
    }
}
