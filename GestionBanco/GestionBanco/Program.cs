﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionBanco
{
    using Busquedas.GUI;
    using GestionCuentas;
    using GestionTransferencias.Core;
    using GestionTransferencias.Core.Transferencias;

    class Program
    {

        static void Main(string[] args)
        {
            RegistroCuentas registroCuentas = new RegistroCuentas().fromXML();
            RegistroTransferencias registroTransferencias = RegistroTransferencias.RecuperarXml(registroCuentas);

            foreach (Transferencia transferencia in registroTransferencias)
            {
                if (transferencia.Tipo == "Periodica")
                {
                    ((Periodica)transferencia).Actualizacion();
                }
            }

            var form = new MainWindowCore().MainWindowViewClientes;
            Application.Run(form);
        }
    }
}
