﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Busquedas.GUI
{
    using Busquedas.Core;
    using Busquedas.GUI.Dlgs;

    using GestionTransferencias;
    using GestionClientes.Core;
    using GestionClientes.View;

    using GestionCuentas;
    using System.Text;
    using GestionCuentas.Cuentas;
    using GestionCuentas.GUI;
    using GestionTransferencias.Core;
    using GestionTransferencias.View;


    class MainWindowCore
    {
        private DlgMovimientos DlgMovimientos;
        private DlgProductos DlgProductos;
        public MainWindowCore()
        {
            //this.MainWindowView = new GestionClientes.View.MainWindowView();
            this.MainWindowViewClientes = new GestionClientes.View.MainWindowView();
            this.MainWindowViewCuentas = new GestionCuentas.GUI.MainWindowView();
            this.MainWindowViewBusquedas = new Busquedas.GUI.MainWindowView();


            this.Cuentas = new RegistroCuentas();
            this.Cuentas = Cuentas.fromXML();
            this.Clientes = RegistroClientes.RecuperarXml();
            this.Transferencias = RegistroTransferencias.RecuperarXml(this.Cuentas);
            this.Search = new Busqueda(this.Clientes, this.Cuentas, this.Transferencias);

            /*
            this.Search = new Busqueda(this.Clientes, this.Cuentas, this.Transferencias);
            this.MainWindowView.FormClosed += (sender, e) => this.OnQuit();
            this.MainWindowView.opSalir.Click += (sender, e) => this.Salir();
            this.MainWindowView.mSearch.Click += (sender, e) => this.BuildSearch();
            */

            //Clientes
            this.gestionClientes();

            //Cuentas
            this.MainWindowViewClientes.OpCuentas.Click += (sender, e) => this.gestionCuentas();

            //Transferencias
            this.MainWindowViewClientes.OpTransferencias.Click += (sender, e) => this.gestionTransferencias();

            //Busquedas
            this.MainWindowViewClientes.OpBuscar.Click += (sender, e) => this.BuildSearch();


        }



        //Ver cuentas de clientes
        private void verCuentas()
        {
            string dni = (string)this.MainWindowViewClientes.grdLista.CurrentRow.Cells[0].Value;
            Cliente cliente = this.Clientes.getCliente(dni);
            Busqueda b = new Busqueda(Clientes,Cuentas,Transferencias);
            List<Cuenta> cuentasDni = b.BuscarCuentas(dni);

            //Transferencias
            double[] arrayT;
            
            List<Transferencia> transCuenta = b.BuscarTransferencias(dni);
            arrayT = new double[transCuenta.Count];
            int i = 0;

            foreach (Transferencia t in transCuenta)
            {

                arrayT[i] = t.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgCuentas.Trans = arrayT;

            //Depositos
            double[] arrayD;
            
            List<Operacion> depCuenta = b.BuscarDepositos(dni);
            arrayD = new double[depCuenta.Count];
            i = 0;

            foreach (Operacion o in depCuenta)
            {

                arrayD[i] = o.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgCuentas.Dep = arrayD;
            //Retiradas
            double[] arrayR;
            List<Operacion> retCuenta = b.BuscarRetiradas(dni);
            arrayR = new double[retCuenta.Count];
            i = 0;

            foreach (Operacion o in retCuenta)
            {

                arrayR[i] = o.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgCuentas.Ret = arrayR;


            //this.dlgCuentas.asignarValores(array);
            this.dlgCuentas = new DlgCuentas(cuentasDni,cliente);
            this.dlgCuentas.GrdLista.Click += (sender, e) => this.clickOpcionesCuentasCliente();
            this.dlgCuentas.Show();
            
            

        }

        //Ver transferencias de cuenta
        private void verTransferenciasCuenta()
        {
            ulong cccCuenta = (ulong) this.dlgCuentas.GrdLista.CurrentRow.Cells[1].Value;
            Cuenta c = this.Cuentas.getCuenta(cccCuenta);
            Busqueda b = new Busqueda(Clientes, Cuentas, Transferencias);
            List<Transferencia> transCuenta = b.BuscarTransferenciasCuenta(c);
            this.dlgTransferencias = new DlgTransferencias(transCuenta, this.dlgCuentas.Cliente);
            this.dlgTransferencias.Show();





           // Busquedas.GUI.Dlgs.DlgCue = array;
        }

        //Ver movimientos de cuenta
        private void verMovimientos()
        {
            ulong cccCuenta = (ulong)this.dlgCuentas.GrdLista.CurrentRow.Cells[1].Value;
            Cuenta c = this.Cuentas.getCuenta(cccCuenta);
            Busqueda b = new Busqueda(Clientes, Cuentas, Transferencias);

            //Transferencias
            double[] arrayT;

            List<Transferencia> transCuenta = b.BuscarTransferenciasCuenta(c);
            arrayT = new double[transCuenta.Count];
            int i = 0;

            foreach (Transferencia t in transCuenta)
            {

                arrayT[i] = t.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgOperaciones.Trans = arrayT;

            //Depositos
            double[] arrayD;

            List<Operacion> depCuenta = c.Depositos;
            arrayD = new double[depCuenta.Count];
            i = 0;

            foreach (Operacion o in depCuenta)
            {

                arrayD[i] = o.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgOperaciones.Dep = arrayD;
            //Retiradas
            double[] arrayR;
            List<Operacion> retCuenta = c.Retiradas;
            arrayR = new double[retCuenta.Count];
            i = 0;

            foreach (Operacion o in retCuenta)
            {

                arrayR[i] = o.Importe;
                i++;
            }
            Busquedas.GUI.Dlgs.DlgOperaciones.Ret = arrayR;

            this.dlgOperaciones = new DlgOperaciones(c, this.dlgCuentas.Cliente);
            this.dlgOperaciones.Show();
        }
        private void gestionClientes()
        {
            TransTot();
            this.Clientes = RegistroClientes.RecuperarXml();
            this.MainWindowViewClientes = new GestionClientes.View.MainWindowView();
            this.MainWindowViewClientes.OpInsertaCliente.Click += (sender, e) => this.InsertarCliente();
            this.MainWindowViewClientes.grdLista.Click += (sender, e) => this.Acciones();
            this.MainWindowViewClientes.OpSalir.Click += (sender, e) => this.Salir();
            this.MainWindowViewClientes.Shown += (sender, e) => this.ActualizaCliente();
            //this.MainWindowViewClientes.Closed += (sender, e) => this.GuardarClientes();
        }

        void TransTot()
        {

            double[] array;
            array = new double[10];

            int i = 0;

            foreach (Transferencia t in this.Transferencias.List)
            {
                Console.WriteLine("Voy a meter un importe: " + t.Importe);
                array[i] = t.Importe;
                i++;
            }

            GestionClientes.View.MainWindowView.Trans = array;
        }

        private void GuardarClientes()
        {
            this.Clientes.GuardarXml();
        }

        //Opciones de tabla cuentas-clientes
        private void clickOpcionesCuentasCliente()
        {
            if (this.dlgCuentas.GrdLista.CurrentCell.ColumnIndex == 7)
            {
                this.EliminarCuentaCliente();
            }
            else if (this.dlgCuentas.GrdLista.CurrentCell.ColumnIndex == 8)
            {
                int fila = this.dlgCuentas.GrdLista.CurrentCell.RowIndex;
                this.ModificarCuentaCliente((ulong)this.dlgCuentas.GrdLista.Rows[fila].Cells[1].Value);
            }
            else if (this.dlgCuentas.GrdLista.CurrentCell.ColumnIndex == 9)
            {
                int fila = this.dlgCuentas.GrdLista.CurrentCell.RowIndex;
                this.verTransferenciasCuenta();
            }
            else if (this.dlgCuentas.GrdLista.CurrentCell.ColumnIndex == 10)
            {
                int fila = this.dlgCuentas.GrdLista.CurrentCell.RowIndex;
                this.verMovimientos();
            }
        }

        private void EliminarCuentaCliente()
        {
            ulong cccEliminar = (ulong)this.dlgCuentas.GrdLista.CurrentRow.Cells[1].Value;

            //Dialogo de confirmación de eiminación
            DialogResult result;
            string mensaje = "¿Está seguro de que desea eliminar la cuenta con CCC( " +
                            cccEliminar + " ), del registro de cuentas?";
            string tittle = "Eliminar cuenta";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            result = MessageBox.Show(mensaje, tittle, buttons);

            if (result == DialogResult.Yes)
            {
                Console.WriteLine("ELIMINAR: " + cccEliminar);
                Cuenta cuenta = Cuentas.getCuenta(cccEliminar);
                this.Cuentas.Remove(cuenta);
                this.dlgCuentas.Cuentas.Remove(cuenta);
                this.dlgCuentas.Actualiza();
            }
        }

        private void ModificarCuentaCliente(ulong cccMod)
        {

            //A partir de la clave de la entidad Cuenta, obtenemos la cuenta a modificar
            Cuenta mod = this.Cuentas.getCuenta(cccMod);
            string tipo = mod.Tipo;
            List<Cliente> titulares = mod.Titulares;

            var dlgModificar = new DlgModificarCuenta(mod);
            if (dlgModificar.ShowDialog() == DialogResult.OK)
            {
                this.Cuentas.Remove(mod);
                this.dlgCuentas.Cuentas.Remove(mod);

                double saldo = dlgModificar.Saldo;
                double interes = dlgModificar.Interes;

                Cuenta c = null;

                switch (tipo)
                {
                    case "CORRIENTE":
                        c = new Corriente(cccMod, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;
                    case "VIVIENDA":
                        c = new Vivienda(cccMod, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;
                    case "AHORRO":
                        c = new Ahorro(cccMod, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;

                }
                this.Cuentas.Add(c);
                this.dlgCuentas.Cuentas.Add(c);

            }
            this.dlgCuentas.Actualiza();
        }

        //MAINWINDOWSCORE DE TRANSFERENCIAS
        private void gestionTransferencias()
        {
            this.Transferencias = RegistroTransferencias.RecuperarXml(this.Cuentas);

            this.MainWindowViewTransferencias = new GestionTransferencias.View.MainWindowView();

            this.MainWindowViewTransferencias.OpInserta.Click += (sender, e) => this.InsertarTransferencia();
            this.MainWindowViewTransferencias.Shown += (sender, e) => this.ActualizaTransferencias();
            this.MainWindowViewTransferencias.Closed += (sender, e) => this.GuardarTransferencia();
            this.MainWindowViewTransferencias.Show();
        }

        void GuardarTransferencia()
        {
            this.Transferencias.GuardarXml();
        }

        void InsertarTransferencia()
        {
            if(this.Cuentas.Count >= 2) { 
                var dlgInserta = new DlgInsertaTransferencia();

                if (dlgInserta.ShowDialog() == DialogResult.OK)
                {
                    var cccOrigen = Cuentas.getCuenta(dlgInserta.CCCorigen);
                    var cccdestino = Cuentas.getCuenta(dlgInserta.CCCdestino);
                    Transferencia trans = Transferencia.Crea(dlgInserta.Tipo, cccOrigen, cccdestino, dlgInserta.Importe);

                    cccOrigen.Saldo = cccOrigen.Saldo - dlgInserta.Importe;
                    cccdestino.Saldo = cccdestino.Saldo + dlgInserta.Importe;
                    this.Transferencias.Add(trans);
                }
                this.ActualizaTransferencias();
            }
            else
            {
                MessageBox.Show("Actualmente existen menos de 2 cuentas en el banco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }



        void ActualizaTransferencias()
        {
            int numElementos = this.Transferencias.Count();
            for (int i = 0; i < numElementos; i++)
            {
                if (this.MainWindowViewTransferencias.grdLista.Rows.Count <= i)
                {
                    this.MainWindowViewTransferencias.grdLista.Rows.Add();
                }
                this.ActualizarFilaListaTransferencias(i);
                this.Transferencias.GuardarXml();
            }

            // Eliminar filas sobrantes
            int numExtra = this.MainWindowViewTransferencias.grdLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.MainWindowViewTransferencias.grdLista.Rows.RemoveAt(numElementos);
            }
            this.Transferencias.GuardarXml();
        }

        private void ActualizarFilaListaTransferencias(int numFila)
        {
            if (numFila < 0
              || numFila > this.MainWindowViewTransferencias.grdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.MainWindowViewTransferencias.grdLista.Rows[numFila];
            Transferencia trans = this.Transferencias[numFila];



            fila.Cells[0].Value = (numFila + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = trans.IdTransferencia;
            fila.Cells[2].Value = trans.Tipo;
            fila.Cells[3].Value = trans.CCCorigen.CCC;
            fila.Cells[4].Value = trans.CCCdestino.CCC;
            fila.Cells[5].Value = trans.Importe;
            fila.Cells[6].Value = trans.Fecha;

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = trans.ToString();
            }

        }

        //MAINWINDOWSCORE DE CUENTAS
        private void gestionCuentas()
        {
            this.MainWindowViewCuentas = new GestionCuentas.GUI.MainWindowView();
            this.ActualizaCuenta();
            this.MainWindowViewCuentas.Shown += (sender, e) => this.ActualizaCuenta();
            this.MainWindowViewCuentas.gridLista.Click += (sender, e) => this.ClickLista();
            this.MainWindowViewCuentas.OpGuardar.Click += (sender, e) => this.GuardarCuenta();
            this.MainWindowViewCuentas.OpAnadir.Click += (sender, e) => this.InsertarCuenta();
            this.MainWindowViewCuentas.opRetirar.Click += (sender, e) => this.RealizarRetirada();
            this.MainWindowViewCuentas.opDepositar.Click += (sender, e) => this.RealizarDeposito();

            this.MainWindowViewCuentas.Show();

        }

        private void RealizarRetirada()
        {
            List<Cuenta> cuentasPosibles = this.Cuentas.cuentasRetirar();
            ulong[] cccCuentasRetirar = new ulong[cuentasPosibles.Count];
            int i = 0;
            foreach (Cuenta c in cuentasPosibles)
            {
                cccCuentasRetirar[i] = c.CCC;
                i++;
            }
            var dlg = new DlgRetirarCuenta(cccCuentasRetirar);
            if (dlg.ShowDialog() == DialogResult.OK)
            {

                double saldo = dlg.Saldo;
                string cccCuenta = dlg.CCC;
                Console.WriteLine("El numero de cuenta es: " + cccCuenta);
                
                if (UInt64.TryParse(cccCuenta, out ulong ccc))
                {
                    Cuenta c = this.Cuentas.getCuenta(ccc);
                    if (c.Saldo >= (c.Saldo - saldo))
                    {
                        Operacion op = new Operacion("RET", saldo, DateTime.Now);
                        c.Retiradas.Add(op);
                        c.Saldo -= saldo;
                    }
                    else
                    {
                        string mensaje = "La cuenta " + c.CCC + "no dispone de saldo suficiente para realizar la retirada";
                        string tittle = "Error al retirar";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        var result = MessageBox.Show(mensaje, tittle, buttons);
                    }


                }
                else
                {
                    string mensaje = "Debe insertar un número de cuenta correcto";
                    string tittle = "Error al retirar";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    var result = MessageBox.Show(mensaje, tittle, buttons);
                }


            }
            this.ActualizaCuenta();
            this.Cuentas.toXML();

        }

        private void RealizarDeposito()
        {
            List<Cuenta> cuentasPosibles = this.Cuentas.List;
            ulong[] cccCuentas = new ulong[cuentasPosibles.Count];
            int i = 0;
            foreach (Cuenta c in cuentasPosibles)
            {
                cccCuentas[i] = c.CCC;
                i++;
            }
            var dlg = new DlgDepositar(cccCuentas);
            if (dlg.ShowDialog() == DialogResult.OK)
            {

                double saldo = dlg.Saldo;
                string cccCuenta = dlg.CCC;

                if (UInt64.TryParse(cccCuenta, out ulong ccc))
                {
                    Cuenta c = this.Cuentas.getCuenta(ccc);
                    Operacion op = new Operacion("DEP", saldo, DateTime.Now);
                    c.Depositos.Add(op);
                    c.Saldo += saldo;    

                }
                else
                {
                    string mensaje = "Debe insertar un número de cuenta correcto";
                    string tittle = "Error al depositar";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    var result = MessageBox.Show(mensaje, tittle, buttons);
                }


            }
            this.ActualizaCuenta();
            this.Cuentas.toXML();

        }

        void GuardarCuenta()
        {
            this.Cuentas.toXML();
        }

        void ClickLista()
        {
            if (this.MainWindowViewCuentas.gridLista.CurrentCell.ColumnIndex == 6)
            {
                this.EliminarCuenta();
            }
            else if (this.MainWindowViewCuentas.gridLista.CurrentCell.ColumnIndex == 7)
            {
                int fila = this.MainWindowViewCuentas.gridLista.CurrentCell.RowIndex;
                this.ModificarCuenta((ulong)this.MainWindowViewCuentas.gridLista.Rows[fila].Cells[0].Value);
            }
        }


        void EliminarCuenta()
        {
            ulong cccEliminar = (ulong)this.MainWindowViewCuentas.gridLista.CurrentRow.Cells[0].Value;

            //Dialogo de confirmación de eiminación
            DialogResult result;
            string mensaje = "¿Está seguro de que desea eliminar la cuenta con CCC( " +
                            cccEliminar + " ), del registro de cuentas?";
            string tittle = "Eliminar cuenta";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            result = MessageBox.Show(mensaje, tittle, buttons);

            if (result == DialogResult.Yes)
            {
                Cuentas.Remove(Cuentas.getCuenta(cccEliminar));
                this.ActualizaCuenta();
            }

        }

        void ModificarCuenta(ulong ccc)
        {

            //A partir de la clave de la entidad Cuenta, obtenemos la cuenta a modificar
            Cuenta mod = this.Cuentas.getCuenta(ccc);
            string tipo = mod.Tipo;
            List<Cliente> titulares = mod.Titulares;


            var dlgModificar = new DlgModificarCuenta(mod);
            if (dlgModificar.ShowDialog() == DialogResult.OK)
            {
                this.Cuentas.Remove(mod);

                double saldo = dlgModificar.Saldo;
                double interes = dlgModificar.Interes;

                Cuenta c = null;

                switch (tipo)
                {
                    case "CORRIENTE":
                        c = new Corriente(ccc, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;
                    case "VIVIENDA":
                        c = new Vivienda(ccc, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;
                    case "AHORRO":
                        c = new Ahorro(ccc, tipo, saldo, mod.Titulares, mod.FechaApertura, interes, mod.Depositos, mod.Retiradas);
                        break;

                }
                this.Cuentas.Add(c);

            }
            this.ActualizaCuenta();
        }

        void InsertarCuenta()
        {
            var dlgInsertar = new DlgInsertarCuenta(this.Cuentas.rgClientes);

            if (dlgInsertar.ShowDialog() == DialogResult.OK)
            {
                ulong ccc = dlgInsertar.CCC;
                if (this.Cuentas.comprobarCCC(ccc))
                {
                    string mensaje = "No se ha insertado la cuenta, porque el CCC ya pertenece a una cuenta del registro";
                    string tittle = "Error al insertar";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    var result = MessageBox.Show(mensaje, tittle, buttons);
                }
                else
                {

                    string tipo = dlgInsertar.Tipo;
                    DateTime fecha = DateTime.Now;

                    double saldo = dlgInsertar.Saldo;
                    double interes = dlgInsertar.Interes;

                    //Obtenemos los clientes seleccionados en el checkedlistbox

                    var claveCliente = new List<string>();
                    List<Cliente> titulares = new List<Cliente>();
                    for (int i = 0; i < dlgInsertar.listTitulares.CheckedItems.Count; i++)
                    {
                        Console.WriteLine("El cliente es: " + dlgInsertar.listTitulares.CheckedItems[i].ToString());
                        claveCliente.Add(dlgInsertar.listTitulares.CheckedItems[i].ToString());
                        titulares.Add(this.Clientes.getCliente(dlgInsertar.listTitulares.CheckedItems[i].ToString()));
                    }

                    Cuenta c = null;

                    switch (tipo)
                    {
                        case "CORRIENTE":
                            c = new Corriente(ccc, tipo, saldo, titulares, fecha, interes, new List<Operacion>(), new List<Operacion>());
                            break;
                        case "VIVIENDA":
                            c = new Vivienda(ccc, tipo, saldo, titulares, fecha, interes, new List<Operacion>(), new List<Operacion>());
                            break;
                        case "AHORRO":
                            c = new Ahorro(ccc, tipo, saldo, titulares, fecha, interes, new List<Operacion>(), new List<Operacion>());
                            break;

                    }
                    this.Cuentas.Add(c);
                }


            }
            this.ActualizaCuenta();
        }

        //Función para obtener los titulares de una cuenta a partir de sus claves obtenidas en el checkedlistbox --> NECESARIO GESTIÓN DE CLIENTES
        List<Cliente> getTitulares(List<string> clavesTitulares)
        {
            List<Cliente> toret = new List<Cliente>();

            foreach (string clave in clavesTitulares)
            {
                Cliente c = Clientes.getCliente(clave);
                toret.Add(c);
            }

            return toret;
        }


        void ActualizaCuenta()
        {
            int numElementos = this.Cuentas.Count;
            for (int i = 0; i < numElementos; i++)
            {
                if (this.MainWindowViewCuentas.gridLista.Rows.Count <= i)
                {
                    this.MainWindowViewCuentas.gridLista.Rows.Add();
                }
                this.ActuaizarFilaCuenta(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.MainWindowViewCuentas.gridLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.MainWindowViewCuentas.gridLista.Rows.RemoveAt(numElementos);
            }
            this.Cuentas.toXML();

        }

        private void ActuaizarFilaCuenta(int numFila)
        {
            if (numFila < 0
              || numFila > this.MainWindowViewCuentas.gridLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.MainWindowViewCuentas.gridLista.Rows[numFila];
            Cuenta cuenta = this.Cuentas[numFila];

            StringBuilder titulares = new StringBuilder();
            if (cuenta.Titulares != null)
            {
                foreach (Cliente cli in cuenta.Titulares)
                {
                    titulares.Append(cli.Nombre + " - ");
                }
            }


            fila.Cells[0].Value = cuenta.CCC;
            fila.Cells[1].Value = cuenta.Tipo;
            fila.Cells[2].Value = cuenta.Saldo;
            fila.Cells[3].Value = cuenta.InteresMensual;
            fila.Cells[4].Value = cuenta.FechaApertura;
            fila.Cells[5].Value = titulares.ToString();
            fila.Cells[6].Value = "o";
            fila.Cells[7].Value = "o";


        }

        //MAINWINDOWSCORE DE BUSQUEDAS

        private void ClickListaMovimientos()
        {

            if (this.DlgMovimientos.GrdLista.CurrentCell.ColumnIndex == 7)
            {
                int row = (int)this.DlgMovimientos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];

                var dlgOp = new DlgOperaciones(cuenta, this.Cliente);
                this.DlgOpen = dlgOp;
                if (dlgOp.ShowDialog() == DialogResult.OK)
                {

                }
            }

        }

        private void ClickListaProductos()
        {

            if (this.DlgProductos.GrdLista.CurrentCell.ColumnIndex == 7)
            {
                int row = (int)this.DlgProductos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];

                var dlgOp = new DlgOperaciones(cuenta, this.Cliente);
                this.DlgOpen = dlgOp;
                if (dlgOp.ShowDialog() == DialogResult.OK)
                {

                }
            }
            else if (this.DlgProductos.GrdLista.CurrentCell.ColumnIndex == 8)
            {
                int row = (int)this.DlgProductos.GrdLista.CurrentRow.Index;
                var cuenta = this.Cuentas.List[row];
                var transf = this.Search.BuscarTransferenciasCuenta(cuenta);
                var dlgTransf = new DlgTransferencias(transf, this.Cliente);
                this.DlgOpen = dlgTransf;
                if (dlgTransf.ShowDialog() == DialogResult.OK)
                {

                }
            }

        }

        void BuildSearch()
        {
            var dlgSearch = new DlgSearch();
            this.DlgOpen = dlgSearch;
            //Si se eligió búsqueda por persona
            if (dlgSearch.ShowDialog() == DialogResult.OK)
            {
                var op = dlgSearch.cbType.SelectedIndex;

                switch (op)
                {
                    case -1:
                        {
                            MessageBox.Show("Seleccione un tipo de búsqueda.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dlgSearch.DialogResult = DialogResult.Cancel;
                            dlgSearch.Close();
                        }
                        break;
                    case 0:
                        {
                            if (this.Clientes.List.Count > 0) //Si hay clientes en el banco
                            {
                                var dlgClient = new DlgSearchClient(this.Clientes); //crea el dialogo para Buscar por persona
                                if (dlgClient.ShowDialog() == DialogResult.OK)
                                {
                                    if ((dlgClient.cbType.SelectedIndex == -1) || (dlgClient.cbClient.SelectedIndex == -1)) //si no se eligió ninguna opción de busqueda
                                    {
                                        MessageBox.Show("No se puede dejar ningún campo en blanco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        dlgClient.DialogResult = DialogResult.Cancel;
                                        dlgClient.Close();
                                    }
                                    else //si se eligieron todas las opciones de busqueda
                                    {
                                        this.SearchClient(dlgClient);
                                    }
                                }
                            }
                            else//si no hay clientes
                            {
                                MessageBox.Show("No existen clientes en el banco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        break;
                    //Si se eligió búsqueda en todo el banco

                    case 1:
                        {
                            if (this.Transferencias.List.Count > 0)//Si hay transferencias en el banco
                            {
                                var listTransf = this.Search.BuscarTransferenciasBanco();
                                var dlgTransf = new DlgTransferencias(listTransf, null);
                                //var listTransf = this.Search.BuscarTransferenciasBanco();
                                if (dlgTransf.ShowDialog() == DialogResult.OK)
                                {

                                }
                            }
                            else
                            {
                                MessageBox.Show("No existen trasferencias en el banco.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        break;
                }
            }
        }

        void SearchClient(DlgSearchClient dlg)
        {
            var op = dlg.cbType.Text;
            var dni = dlg.cbClient.Text.Split('-')[1].ToString();
            this.Cliente = Search.GetClient(dni);
            switch (op)
            {
                case "Productos":
                    {
                        var listCuentas = this.Search.BuscarCuentas(dni);
                        var dlgProd = new DlgProductos(listCuentas, this.Cliente);
                        this.DlgProductos = dlgProd;
                        this.DlgProductos.GrdLista.Click += (sender, e) => this.ClickListaProductos();

                        if (dlgProd.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }
                    break;
                case "Movimientos":
                    {
                        var listCuentas = this.Search.BuscarCuentas(dni);
                        var dlgMovs = new DlgMovimientos(listCuentas, this.Cliente);
                        this.DlgMovimientos = dlgMovs;
                        this.DlgMovimientos.GrdLista.Click += (sender, e) => this.ClickListaMovimientos();

                        if (dlgMovs.ShowDialog() == DialogResult.OK)
                        {


                        }
                    }
                    break;
                case "Transferencias":
                    {
                        var listTransf = this.Search.BuscarTransferencias(dni);
                        var dlgTransf = new DlgTransferencias(listTransf, this.Cliente);
                        this.DlgOpen = dlgTransf;
                        if (dlgTransf.ShowDialog() == DialogResult.OK)
                        {

                        }
                    }
                    break;
            }
        }

        //MAINWINDOWSCORE DE CLIENTES

        void InsertarCliente()
        {
            var dlgInserta = new DlgInsertaCliente();
            
            if (dlgInserta.ShowDialog() == DialogResult.OK)
            {
                var dnis = Clientes.getDnis();
                bool existe = false;
                foreach (String d in dnis)
                {
                    if (dlgInserta.Dni == d)
                    {
                        existe = true;
                    }
                }
                if (existe == false){
                    Cliente cliente = new Cliente(dlgInserta.Dni, dlgInserta.Nombre, dlgInserta.Telefono, dlgInserta.Email, dlgInserta.Direccion);
                    this.Clientes.Add(cliente);
                }else{
                    DialogResult result;
                    string mensaje = "Error al insertar, ya existe el DNI:" + dlgInserta.Dni + ".";
                    string tittle = "Atención";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    result = MessageBox.Show(mensaje, tittle, buttons);
                }
            }
            this.ActualizaCliente();
            this.Clientes.GuardarXml();
        }

        void Acciones()
        {
            if (this.MainWindowViewClientes.grdLista.CurrentCell.ColumnIndex == 5)
            {
                this.EliminarCliente();
            }
            else if (this.MainWindowViewClientes.grdLista.CurrentCell.ColumnIndex == 6)
            {
                string dni = (string)this.MainWindowViewClientes.grdLista.CurrentRow.Cells[0].Value;
                this.ModificarCliente(dni);
            }else if (this.MainWindowViewClientes.grdLista.CurrentCell.ColumnIndex == 7)
            {
                /*
                DialogResult result;
                string mensaje = "Ver cuentas.";
                string tittle = "Cuentas";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                result = MessageBox.Show(mensaje, tittle, buttons);
                */
                this.verCuentas();
            }
        }

        void EliminarCliente()
        {
            string dni = (string)this.MainWindowViewClientes.grdLista.CurrentRow.Cells[0].Value;
            DialogResult result;
            string mensaje = "¿Seguro que quieres eliminar el cliente " + dni + "?";
            string tittle = "Eliminar Cliente";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            result = MessageBox.Show(mensaje, tittle, buttons);

            if (result == DialogResult.Yes)
            {
                Clientes.Remove(Clientes.getCliente(dni));
                this.ActualizaCliente();
            }

        }

        void ModificarCliente(string dni)
        {
            Cliente cliente = this.Clientes.getCliente(dni);
            var dlgModificarCliente = new DlgModificarCliente(cliente);
            if (dlgModificarCliente.ShowDialog() == DialogResult.OK)
            {
                this.Clientes.Remove(cliente);

                Cliente clienteNuevo = new Cliente(dni,
                                                   dlgModificarCliente.Nombre,
                                                   dlgModificarCliente.Telefono, 
                                                   dlgModificarCliente.Email, 
                                                   dlgModificarCliente.Direccion);
                this.Clientes.Add(clienteNuevo);

            }
            this.ActualizaCliente();
        }


        void ActualizaCliente()
        {
            int numElementos = this.Clientes.Count;
            for (int i = 0; i < numElementos; i++)
            {
                if (this.MainWindowViewClientes.grdLista.Rows.Count <= i)
                {
                    this.MainWindowViewClientes.grdLista.Rows.Add();
                }
                this.ActualizarFilaListaCliente(i);
                this.Clientes.GuardarXml();
            }

            // Eliminar filas sobrantes
            int numExtra = this.MainWindowViewClientes.grdLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.MainWindowViewClientes.grdLista.Rows.RemoveAt(numElementos);
            }
            this.Clientes.GuardarXml();
        }

        private void ActualizarFilaListaCliente(int numFila)
        {
            if (numFila < 0
              || numFila > this.MainWindowViewClientes.grdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.MainWindowViewClientes.grdLista.Rows[numFila];
            Cliente cliente = this.Clientes[numFila];

			fila.Cells[0].Value = cliente.DNI;
			fila.Cells[1].Value = cliente.Nombre;
			fila.Cells[2].Value = cliente.Telefono;
			fila.Cells[3].Value = cliente.Email;
			fila.Cells[4].Value = cliente.DireccionPostal;
            fila.Cells[5].Value = "o";
            fila.Cells[6].Value = "o";
            fila.Cells[7].Value = "o";

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = cliente.ToString();
            }
            
        }


        void OnQuit()
        {
            Application.Exit();
        }

        void Salir()
        {
            this.Clientes.GuardarXml();
            Application.Exit();
        }
        public MainWindowView MainWindowView
        {
            get; private set;
        }
         public GestionClientes.View.MainWindowView MainWindowViewClientes
        {
            get; private set;
        }
        public GestionCuentas.GUI.MainWindowView MainWindowViewCuentas
        {
            get; private set;
        }
        public GestionTransferencias.View.MainWindowView MainWindowViewTransferencias
        {
            get; private set;
        }
        public Busquedas.GUI.MainWindowView MainWindowViewBusquedas
        {
            get; private set;
        }
        private Dlg DlgOpen;
        private RegistroCuentas Cuentas { get; set; }
        private RegistroClientes Clientes { get; set; }
        private RegistroTransferencias Transferencias { get; set; }
        private Busqueda Search { get; set; }
        private Cliente Cliente;

        private DlgCuentas dlgCuentas;
        private DlgTransferencias dlgTransferencias;
        private DlgOperaciones dlgOperaciones;
    }
}
