﻿namespace GestionClientes.View
{
    using Core;
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public class DlgModificarCliente : Form
    {
        private Cliente c;

        public DlgModificarCliente(Cliente c)
        {
            this.c = c;
            this.Build();
        }

        public Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Guardar"
            };

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }

        Panel buildDNI()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edDni = new TextBox {
                Dock = DockStyle.Fill, 
                TextAlign = HorizontalAlignment.Center, 
                Text = this.c.DNI, 
                ReadOnly = true 
            };
            var lbDni = new Label
            {
                Text = "DNI:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edDni);
            toret.Controls.Add(lbDni);
            toret.MaximumSize = new Size(int.MaxValue, edDni.Height * 2);

            return toret;
        }

        Panel buildNombre()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edNombre = new TextBox { 
                Dock = DockStyle.Fill, 
                TextAlign = HorizontalAlignment.Center, 
                Text = this.c.Nombre 
            };
            var lbNombre = new Label
            {
                Text = "Nombre:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edNombre);
            toret.Controls.Add(lbNombre);
            toret.MaximumSize = new Size(int.MaxValue, edNombre.Height * 2);

            return toret;
        }

        Panel buildTelefono()
        {
            var toret = new Panel();
            this.edTelefono = new TextBox
            {
                Text = System.Convert.ToString(c.Telefono),
                TextAlign = HorizontalAlignment.Center,
                Dock = DockStyle.Fill
            };

            var lbTelefono = new Label
            {
                Text = "Telefono: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edTelefono);
            toret.Controls.Add(lbTelefono);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edTelefono.Height * 2);

            return toret;
        }

        Panel buildEmail()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edEmail = new TextBox
            {
                Dock = DockStyle.Fill,
                TextAlign = HorizontalAlignment.Center,
                Text = this.c.Email
            };
            var lbEmail = new Label
            {
                Text = "Email:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edEmail);
            toret.Controls.Add(lbEmail);
            toret.MaximumSize = new Size(int.MaxValue, edEmail.Height * 2);

            return toret;
        }

        Panel buildDireccionPostal()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edDireccion = new TextBox
            {
                Dock = DockStyle.Fill,
                TextAlign = HorizontalAlignment.Center,
                Text = this.c.DireccionPostal
            };
            var lbDireccion = new Label
            {
                Text = "Dirección Postal:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edDireccion);
            toret.Controls.Add(lbDireccion);
            toret.MaximumSize = new Size(int.MaxValue, edDireccion.Height * 2);

            return toret;
        }

        void Build()
        {
            this.SuspendLayout();

            var panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            panelInserta.SuspendLayout();
            this.Controls.Add(panelInserta);

            var panelDNI = this.buildDNI();
            panelInserta.Controls.Add(panelDNI);
            var panelNombre = this.buildNombre();
            panelInserta.Controls.Add(panelNombre);
            var panelTelefono = this.buildTelefono();
            panelInserta.Controls.Add(panelTelefono);
            var panelEmail = this.buildEmail();
            panelInserta.Controls.Add(panelEmail);
            var panelDireccion = this.buildDireccionPostal();
            panelInserta.Controls.Add(panelDireccion);

            var panelBotones = this.BuildPnlBotones();
            panelInserta.Controls.Add(panelBotones);

            panelInserta.ResumeLayout(true);

            this.Text = "Modificar cliente";
            this.Size = new Size(400,
                            panelDNI.Height + panelNombre.Height + panelTelefono.Height
                            + panelEmail.Height + panelDireccion.Height + panelBotones.Height);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }


        private TextBox edDni;
        private TextBox edNombre;
        private TextBox edTelefono;
        private TextBox edEmail;
        private TextBox edDireccion;


        public string Nombre => this.edNombre.Text;
        public int Telefono => System.Convert.ToInt32(this.edTelefono.Text);
        public string Email => this.edEmail.Text;
        public string Direccion => this.edDireccion.Text;
    }
}

