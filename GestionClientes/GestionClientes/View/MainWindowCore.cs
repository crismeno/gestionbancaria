﻿namespace GestionClientes.View
{
	using System;
    using System.Windows.Forms;
    using Core;

    public class MainWindowCore
    {
        public MainWindowCore()
        {
			this.registro = RegistroClientes.RecuperarXml();
            this.MainWindowsView = new MainWindowView();
            this.MainWindowsView.ResizeWindow();
            this.MainWindowsView.OpInsertaCliente.Click += (sender, e) => this.InsertarCliente();
            this.MainWindowsView.grdLista.Click += (sender, e) => this.Acciones();
			this.MainWindowsView.OpSalir.Click += (sender, e) => this.Salir();
            this.MainWindowsView.Shown += (sender, e) => this.Actualiza();
            this.MainWindowsView.Closed += (sender, e) => this.Salir();
        }

        void InsertarCliente()
        {
            var dlgInserta = new DlgInsertaCliente();
            
            if (dlgInserta.ShowDialog() == DialogResult.OK)
            {
                var dnis = registro.getDnis();
                bool existe = false;
                foreach (String d in dnis)
                {
                    if (dlgInserta.Dni == d)
                    {
                        existe = true;
                    }
                }
                if (existe == false){
                    Cliente cliente = new Cliente(dlgInserta.Dni, dlgInserta.Nombre, dlgInserta.Telefono, dlgInserta.Email, dlgInserta.Direccion);
                    this.registro.Add(cliente);
                }else{
                    DialogResult result;
                    string mensaje = "Error al insertar, ya existe el DNI:" + dlgInserta.Dni + ".";
                    string tittle = "Atención";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    result = MessageBox.Show(mensaje, tittle, buttons);
                }
            }
            this.Actualiza();
        }

        void Acciones()
        {
            if (this.MainWindowsView.grdLista.CurrentCell.ColumnIndex == 5)
            {
                this.Eliminar();
            }
            else if (this.MainWindowsView.grdLista.CurrentCell.ColumnIndex == 6)
            {
                string dni = (string)this.MainWindowsView.grdLista.CurrentRow.Cells[0].Value;
                this.Modificar(dni);
            }else if (this.MainWindowsView.grdLista.CurrentCell.ColumnIndex == 7)
            {
                DialogResult result;
                string mensaje = "Ver cuentas.";
                string tittle = "Cuentas";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                result = MessageBox.Show(mensaje, tittle, buttons);
            }
        }

        void Eliminar()
        {
            string dni = (string)this.MainWindowsView.grdLista.CurrentRow.Cells[0].Value;
            DialogResult result;
            string mensaje = "¿Seguro que quieres eliminar el cliente " + dni + "?";
            string tittle = "Eliminar Cliente";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            result = MessageBox.Show(mensaje, tittle, buttons);

            if (result == DialogResult.Yes)
            {
                registro.Remove(registro.getCliente(dni));
                this.Actualiza();
            }

        }

        void Modificar(string dni)
        {
            Cliente cliente = this.registro.getCliente(dni);
            var dlgModificarCliente = new DlgModificarCliente(cliente);
            if (dlgModificarCliente.ShowDialog() == DialogResult.OK)
            {
                this.registro.Remove(cliente);

                Cliente clienteNuevo = new Cliente(dni,
                                                   dlgModificarCliente.Nombre,
                                                   dlgModificarCliente.Telefono, 
                                                   dlgModificarCliente.Email, 
                                                   dlgModificarCliente.Direccion);
                this.registro.Add(clienteNuevo);

            }
            this.Actualiza();
        }


        void Actualiza()
        {
            int numElementos = this.registro.Count;
            for (int i = 0; i < numElementos; i++)
            {
                if (this.MainWindowsView.grdLista.Rows.Count <= i)
                {
                    this.MainWindowsView.grdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.MainWindowsView.grdLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.MainWindowsView.grdLista.Rows.RemoveAt(numElementos);
            }
        }

        private void ActualizarFilaLista(int numFila)
        {
            if (numFila < 0
              || numFila > this.MainWindowsView.grdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.MainWindowsView.grdLista.Rows[numFila];
            Cliente cliente = this.registro[numFila];

			fila.Cells[0].Value = cliente.DNI;
			fila.Cells[1].Value = cliente.Nombre;
			fila.Cells[2].Value = cliente.Telefono;
			fila.Cells[3].Value = cliente.Email;
			fila.Cells[4].Value = cliente.DireccionPostal;
            fila.Cells[5].Value = "o";
            fila.Cells[6].Value = "o";
            fila.Cells[7].Value = "o";

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = cliente.ToString();
            }
            
        }

        void Salir()
        {
            this.registro.GuardarXml();
            Application.Exit();
        }



        public MainWindowView MainWindowsView
        {
            get; private set;
        }
		public RegistroClientes registro;

    }


}
