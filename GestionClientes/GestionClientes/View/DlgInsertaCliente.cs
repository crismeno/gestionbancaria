﻿namespace GestionClientes.View
{
	using System.Drawing;
    using System.Windows.Forms;
    
    public class DlgInsertaCliente : Form
    {
        public DlgInsertaCliente()
        {
            this.Build();
        }

        public Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Guardar"
            };
            this.AcceptButton = btGuarda;
            this.CancelButton = btCierra;

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }

		Panel buildDNI()
        {
            var toret = new Panel { Dock = DockStyle.Top };
			this.edDni = new TextBox { Dock = DockStyle.Fill };
            var lbDni = new Label
            {
				Text = "DNI:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edDni);
            toret.Controls.Add(lbDni);
			toret.MaximumSize = new Size(int.MaxValue, edDni.Height * 2);

            this.edDni.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrEmpty(this.Dni);

                if (invalid || !char.IsNumber(this.Dni[0]))
                {
                    this.edDni.Text = "¿DNI?";
                }

                btAccept.Enabled = !invalid;
            };

            return toret;
        }
        
		Panel buildNombre()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edNombre = new TextBox { Dock = DockStyle.Fill };
            var lbNombre = new Label
            {
                Text = "Nombre:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edNombre);
            toret.Controls.Add(lbNombre);
            toret.MaximumSize = new Size(int.MaxValue, edNombre.Height * 2);

            this.edNombre.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.Nombre);

                invalid = invalid || !char.IsLetter(this.Nombre[0]);

                if (invalid)
                {
                    this.edNombre.Text = "¿Nombre?";
                }

                btAccept.Enabled = !invalid;
            };

            return toret;
        }
        
        Panel buildTelefono()
        {
            var toret = new Panel();
            this.edTelefono = new NumericUpDown
            {
                
                TextAlign = HorizontalAlignment.Left,
                Dock = DockStyle.Fill,
                Minimum = 600000000,
				Maximum = 999999999
            };

            var lbTelefono = new Label
            {
                Text = "Telefono: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edTelefono);
            toret.Controls.Add(lbTelefono);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edTelefono.Height * 2);

            return toret;
        }

        Panel buildEmail()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edEmail = new TextBox { Dock = DockStyle.Fill };
            var lbEmail = new Label
            {
                Text = "Email:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edEmail);
            toret.Controls.Add(lbEmail);
            toret.MaximumSize = new Size(int.MaxValue, edEmail.Height * 2);

            this.edEmail.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.Email);

                invalid = invalid || !char.IsLetter(this.Email[0]);

                if (invalid)
                {
                    this.edEmail.Text = "¿Email?";
                }

                btAccept.Enabled = !invalid;
            };

            return toret;
        }

		Panel buildDireccionPostal()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edDireccion = new TextBox { Dock = DockStyle.Fill };
            var lbDireccion = new Label
            {
                Text = "Dirección Postal:",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edDireccion);
            toret.Controls.Add(lbDireccion);
            toret.MaximumSize = new Size(int.MaxValue, edDireccion.Height * 2);

            this.edDireccion.Validating += (sender, cancelArgs) => {
                var btAccept = (Button)this.AcceptButton;
                bool invalid = string.IsNullOrWhiteSpace(this.Direccion);

                invalid = invalid || !char.IsLetter(this.Direccion[0]);

                if (invalid)
                {
                    this.edDireccion.Text = "¿Dirección postal?";
                }

                btAccept.Enabled = !invalid;
            };

            return toret;
        }

        void Build()
        {
            this.SuspendLayout();

            var panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            panelInserta.SuspendLayout();
            this.Controls.Add(panelInserta);
            
            var panelDNI = this.buildDNI();
            panelInserta.Controls.Add(panelDNI);
            var panelNombre = this.buildNombre();
            panelInserta.Controls.Add(panelNombre);
            var panelTelefono = this.buildTelefono();
			panelInserta.Controls.Add(panelTelefono);
            var panelEmail = this.buildEmail();
            panelInserta.Controls.Add(panelEmail);
			var panelDireccion = this.buildDireccionPostal();
			panelInserta.Controls.Add(panelDireccion);
            
            var panelBotones = this.BuildPnlBotones();
            panelInserta.Controls.Add(panelBotones);
            
            panelInserta.ResumeLayout(true);

            this.Text = "Nuevo Cliente";
            this.Size = new Size(400,
                            panelDNI.Height + panelNombre.Height + panelTelefono.Height 
	                        + panelEmail.Height + panelDireccion.Height + panelBotones.Height);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        
        private TextBox edDni;
		private TextBox edNombre;
		private NumericUpDown edTelefono;
        private TextBox edEmail;
		private TextBox edDireccion;
        
        
        public string Dni => this.edDni.Text;
		public string Nombre => this.edNombre.Text;
		public int Telefono => (int)this.edTelefono.Value;
        public string Email => this.edEmail.Text;
		public string Direccion => this.edDireccion.Text;      
    }
}

