﻿namespace GestionClientes.View
{
    using System.Windows.Forms;
    using System.Drawing;

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using System.Windows.Forms.DataVisualization.Charting;


    public class MainWindowView : Form
    {
        

        public Series trans2 = new Series();

        public static double[] Trans = new double[10];


        public MainWindowView()
        {
            this.Build();
            this.Font = new Font("Century Gothic", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            

        }


        void Build()
        {

            this.BuildMenu();
            this.InitializeComponent();

            Panel ListPanel = this.BuildListPanel();

            this.SuspendLayout();

            this.pnlPpal = new Panel()
            {
                Dock = DockStyle.Fill
            };

            this.pnlPpal.SuspendLayout();
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.Controls.Add(ListPanel);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(500, 550);
            this.Text = "Gestión Banco";
            this.Resize += (obj, e) => this.ResizeWindow();

            this.ResizeWindow();
            this.ResumeLayout(true);

        }

        public void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.pnlPpal.ClientRectangle.Width;

            // Redimensionar la tabla
            this.grdLista.Width = width;

            this.grdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .13);
            this.grdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .13);
            this.grdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .13);
            this.grdLista.Columns[3].Width =
                                (int)System.Math.Floor(width * .18);
            this.grdLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .13);
            this.grdLista.Columns[5].Width =
                                (int)System.Math.Floor(width * .10);
            this.grdLista.Columns[6].Width =
                                (int)System.Math.Floor(width * .10);
            this.grdLista.Columns[7].Width =
                                (int)System.Math.Floor(width * .10);

            this.grdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.grdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.PeachPuff;


        }

        void BuildMenu()
        {
            this.Mpal = new MainMenu();

            this.MInsertar = new MenuItem("Insertar");
            this.MGestiones = new MenuItem("Gestiones");


            this.OpInsertaCliente = new MenuItem("Insertar cliente");

            this.OpBuscar = new MenuItem("Busquedas");
            this.OpCuentas = new MenuItem("Cuentas");
            this.OpTransferencias = new MenuItem("Transferencia");

            this.OpSalir = new MenuItem("Salir");

            //this.OpGraficos = new MenuItem("Graficos");

            this.MInsertar.MenuItems.Add(this.OpInsertaCliente);
           
            this.MGestiones.MenuItems.Add(this.OpBuscar);
            this.MGestiones.MenuItems.Add(this.OpCuentas);
            this.MGestiones.MenuItems.Add(this.OpTransferencias);

            this.Mpal.MenuItems.Add(MInsertar);
            this.Mpal.MenuItems.Add(MGestiones);
            //this.Mpal.MenuItems.Add(OpGraficos);
            this.Mpal.MenuItems.Add(OpSalir);
            
            this.Menu = Mpal;
           

        }

        Panel BuildListPanel()
        {
            Panel pnlLista = new Panel();
            pnlLista.SuspendLayout();
            pnlLista.Dock = DockStyle.Fill;

            // Crear gridview
            this.grdLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };

            this.grdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.grdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.White;

            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate1 = new DataGridViewTextBoxCell();
            textCellTemplate0.Style.BackColor = Color.White;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.BackColor = Color.Wheat;
            textCellTemplate1.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.TopCenter;

            var col0 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate0,
                HeaderText = "DNI",
                Width = 50,
                ReadOnly = true
            };

            var col1 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Nombre",
                Width = 50,
                ReadOnly = true
            };

            var col2 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Telefono",
                Width = 50,
                ReadOnly = true
            };

            var col3 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Email",
                Width = 50,
                ReadOnly = true
            };

            var col4 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Dirección",
                Width = 50,
                ReadOnly = true
            };

            var col5 = new DataGridViewButtonColumn
            {
                HeaderText = "Eliminar",
                Width = 50,
                ReadOnly = true
            };

            var col6 = new DataGridViewButtonColumn
            {
                HeaderText = "Modificar",
                Width = 50,
                ReadOnly = true
            };

            var col7 = new DataGridViewButtonColumn
            {
                HeaderText = "Cuentas",
                Width = 50,
                ReadOnly = true
            };

            this.grdLista.Columns.AddRange(new DataGridViewColumn[] {
            col0, col1, col2, col3, col4, col5, col6, col7
        });
            
            pnlLista.Controls.Add(this.grdLista);
            return pnlLista;
           
        }

        public MainMenu Mpal { get; set; }
        public MenuItem MInsertar { get; set; }
        public MenuItem MGestiones { get; set; }
        public MenuItem OpInsertaCliente { get; set; }
        public MenuItem OpBuscar { get; set; }
        public MenuItem OpCuentas { get; set; }
        public MenuItem OpTransferencias { get; set; }
        public MenuItem OpGraficos { get; set; }
        public MenuItem OpSalir { get; set; }




        public Panel pnlPpal;
        public DataGridView grdLista;
        private Chart chart1;



        public void InitializeComponent()
        {
            ChartArea chartArea1 = new ChartArea();
            Legend legend1 = new Legend();




            this.chart1 = new Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();




            this.chart1.BackColor = System.Drawing.Color.PeachPuff;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(45, 170);
            this.chart1.Name = "chart1";



            foreach (double importe in Trans)
            {
                trans2.Points.AddY(importe);
            }


            trans2.YValueType = ChartValueType.Double;

            trans2.ChartArea = "ChartArea1";
            trans2.Legend = "Legend1";
            trans2.Name = "Tranferencias";


            this.chart1.Series.Add(trans2);
            this.chart1.Size = new System.Drawing.Size(804, 300);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // IngresosGeneral
            // 
            this.ClientSize = new System.Drawing.Size(928, 406);
            this.Controls.Add(this.chart1);
            this.Name = "IngresosGeneral";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            //this.Font = new System.Drawing.Font("Century Gothic", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        }

        
    }
}



