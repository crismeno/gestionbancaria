﻿using GestionClientes.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionClientes
{
    class Program
    {
        static void Main(string[] args)
        {
            var form = new MainWindowCore().MainWindowsView;
            Application.Run(form);
        }
    }
}

