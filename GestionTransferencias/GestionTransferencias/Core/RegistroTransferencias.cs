﻿using GestionTransferencias.Core.Transferencias;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using GestionTransferencias.Core;

namespace GestionTransferencias.Core
{
    using GestionCuentas;

    public class RegistroTransferencias: ICollection<Transferencia>
    {
        public const string ArchivoXML = "transferencias.xml";
        public const string LblTransferencias = "transferencias";
        public const string LblTransferencia = "transferencia";
        public const string LblIdTransferencia = "idTransferencia";
        public const string LblTipo = "tipo";
        public const string LblCCCorigen = "CCCorigen";
        public const string LblCCCdestino = "CCCdestino";
        public const string LblImporte = "importe";
        public const string LblFecha = "fecha";


        public const string LblPuntual = "puntual";
        public const string LblPeriodica = "periodica";


        private List<Transferencia> transferencias;

        public List<Transferencia> List
        {
            get { return transferencias; }
        }


        public RegistroTransferencias(RegistroCuentas rc)
        {
            this.transferencias = new List<Transferencia>();
            this.registroCuentas = rc;
        }

        public RegistroTransferencias(IEnumerable<Transferencia> transferencias) 
        {
            this.transferencias.AddRange(transferencias);
        }

        public int Count
        {
            get { return this.transferencias.Count; }
        }

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(Transferencia transferencia)
        {
            this.transferencias.Add(transferencia);
        }

        public void Clear()
        {
            this.transferencias.Clear();
        }

        public bool Contains(Transferencia transferencia)
        {
            return this.transferencias.Contains(transferencia);
        }

        public void CopyTo(Transferencia[] transferencia, int i)
        {
            this.transferencias.CopyTo(transferencia, i);
        }

        public IEnumerator<Transferencia> GetEnumerator()
        {
            foreach (var reparacion in this.transferencias)
            {
                yield return reparacion;
            }
        }

        public bool Remove(Transferencia reparacion)
        {
            return this.transferencias.Remove(reparacion);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var reparacion in this.transferencias)
            {
                yield return reparacion;
            }
        }

        //Indizador
        public Transferencia this[int i]
        {
            get { return this.transferencias[i]; }
            set { this.transferencias[i] = value; }
        }

        public override string ToString()
        {
            var toret = new StringBuilder();
            foreach (Transferencia r in transferencias)
            {
                toret.Append(r);
                toret.AppendLine();
            }

            return toret.ToString();
        }

        //Guarda la lista de transferencias en un xml
        public void GuardarXml()
        {
            this.GuardarXml(ArchivoXML);
        }

        public void GuardarXml(String n)
        {

            var doc = new XDocument();
            var root = new XElement(LblTransferencias);

            foreach (Transferencia t in transferencias)
            {
                XElement transfer = new XElement(LblTransferencia);
                XElement idTransferencia = new XElement(LblIdTransferencia, t.IdTransferencia);
                XElement tipo = new XElement(LblTipo, t.Tipo);
                XElement CCCorigen = new XElement(LblCCCorigen, t.CCCorigen.CCC);
                XElement CCCdestino = new XElement(LblCCCdestino, t.CCCdestino.CCC);
                XElement importe = new XElement(LblImporte, t.Importe);
                XElement fecha = new XElement(LblFecha, t.Fecha);

                XElement e;

                if (t.GetType() == typeof(Puntual))
                {
                    e = PuntualToXml((Puntual)t);
                }
                else if (t.GetType() == typeof(Periodica))
                {
                    e = PeriodicaToXml((Periodica)t);
                }
                else{
                    e = null;
                }
                

                transfer.Add(e);
                transfer.Add(idTransferencia);
                transfer.Add(tipo);
                transfer.Add(CCCorigen);
                transfer.Add(CCCdestino);
                transfer.Add(importe);
                transfer.Add(fecha);

                root.Add(transfer);
            }

            doc.Add(root);
            doc.Save(n);
        }

        public XElement PuntualToXml(Puntual a)
        {
            return new XElement(LblPuntual,
                new XAttribute(LblIdTransferencia, a.IdTransferencia),
                new XAttribute(LblFecha, a.Fecha),
                new XAttribute(LblCCCorigen, a.CCCorigen.CCC),
                new XAttribute(LblCCCdestino, a.CCCdestino.CCC),
                new XAttribute(LblImporte, a.Importe)

            );
        }

        public XElement PeriodicaToXml(Periodica a)
        {
            return new XElement(LblPuntual,
                new XAttribute(LblIdTransferencia, a.IdTransferencia),
                new XAttribute(LblFecha, a.Fecha),
                new XAttribute(LblCCCorigen, a.CCCorigen.CCC),
                new XAttribute(LblCCCdestino, a.CCCdestino.CCC),
                new XAttribute(LblImporte, a.Importe)

            );
        }

        

        public static RegistroTransferencias RecuperarXml(RegistroCuentas rc)
        {
            return RecuperarXml(ArchivoXML, rc);
        }

        //Recupera los datos de un archivo xml
        public static RegistroTransferencias RecuperarXml(String n, RegistroCuentas rc)
        {
            var toret = new RegistroTransferencias( rc );

            try
            {
                var doc = XDocument.Load(n);
                if (doc.Root != null && doc.Root.Name == LblTransferencias)
                {
                    var transferencias = doc.Root.Elements(LblTransferencia);
                    foreach (XElement trans in transferencias)
                    {
                        var elems = trans.Elements();
                        int idTrans = 0;
                        string tip = null;
                        ulong CCCorig = 0;
                        ulong CCCdest = 0;
                        double imp = 0.0;
                        DateTime fech= DateTime.Today;

                        foreach (XElement elem in elems)
                        {
                            if (elem.Name == LblIdTransferencia)
                            {
                                idTrans = (int)trans.Element(LblIdTransferencia);
                            }
                            else if (elem.Name == LblCCCorigen)
                            {
          
                                CCCorig = Convert.ToUInt64(elem.Value);
     
          
                            }
                            else if (elem.Name == LblCCCdestino)
                            {

                                CCCdest = Convert.ToUInt64(elem.Value);

                            }
                            else if (elem.Name == LblImporte)
                            {
                                imp = (double)trans.Element(LblImporte);
                            }

                            else if (elem.Name == LblTipo)
                            {
                                tip = (string)trans.Element(LblTipo);
                            }
                            else if (elem.Name == LblFecha)
                            {
                                fech = (DateTime)trans.Element(LblFecha);
                                //fech = DateTime.ParseExact(LblFecha, "yyyy-MM-dd HH:mm:ss,fff", System.Globalization.CultureInfo.InvariantCulture);
                            }

                        }
                        
                        Transferencia transNueva = Transferencia.Crea(tip, rc.getCuenta(CCCorig), rc.getCuenta(CCCdest),imp, fech);
                        toret.Add(transNueva);

                    }
                }
            }
            catch (XmlException)
            {
                toret.Clear();
            }
            catch (IOException)
            {
                toret.Clear();
            }

            Console.WriteLine( "Transferencias recuperadas" );
            Console.WriteLine(toret);
            return toret;

        }

        public RegistroCuentas registroCuentas;
    }
}
