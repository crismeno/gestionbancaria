﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTransferencias.Core
{
    using GestionCuentas;
    using GestionTransferencias.Core.Transferencias;



    public abstract class Transferencia
    {
        public static int i=0;
        
        protected Transferencia(int idTransferencia, String tipo, Cuenta CCCorigen, Cuenta CCCdestino, double importe, DateTime fecha)
        {
            this.IdTransferencia = idTransferencia;
            this.Tipo = tipo;
            this.CCCorigen = CCCorigen;
            this.CCCdestino = CCCdestino;
            this.Importe = importe;
            this.Fecha = fecha;
        }

        public int IdTransferencia { get; private set; }
        public string Tipo { get; private set; }
        public Cuenta CCCorigen { get; private set; }
        public Cuenta CCCdestino { get; private set; }
        public double Importe { get; private set; }
        public DateTime Fecha { get; private set; }

        public static Transferencia Crea( String tipo, Cuenta CCCorigen, Cuenta CCCdestino, double importe)
        {
            Transferencia toret = null;
            DateTime f = DateTime.Now;

            if (tipo == "Periodica")
            {
                toret = new Periodica(i, CCCorigen, CCCdestino, importe, f);
                i++;
            }else if (tipo == "Puntual")
            {
                toret = new Puntual(i, CCCorigen, CCCdestino, importe, f);
                i++;
            }
            else
            {
                throw new Exception("Error, tiene que indicar el tipo");
            }
            return toret;
        }

        public static Transferencia Crea(String tipo, Cuenta CCCorigen, Cuenta CCCdestino, double importe, DateTime fecha)
        {
            Transferencia toret = null;

            if (tipo == "Periodica")
            {
                toret = new Periodica(i, CCCorigen, CCCdestino, importe, fecha);
                i++;
            }
            else if (tipo == "Puntual")
            {
                toret = new Puntual(i, CCCorigen, CCCdestino, importe, fecha);
                i++;
            }
            else
            {
                throw new Exception("Error, tiene que indicar el tipo");
            }
            return toret;
        }

        public override string ToString()
        {
            return "Transferencia:"+ "\nId de la transferencia:\t" + IdTransferencia+ "\nCuenta Origen:\t"+ CCCorigen + "\nCuenta Destino:\t" + CCCdestino + "\nImporte:\t"+ Importe ;
        }


    }


}
