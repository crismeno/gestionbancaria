﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTransferencias.Core.Transferencias
{
    using GestionCuentas.Cuentas;
    using GestionCuentas;
    public class Puntual : Transferencia
    {
        public const string tipo = "Puntual";

        public Puntual(int idTransferencia, Cuenta CCCorigen, Cuenta CCCdestino, double importe, DateTime fecha)
            : base(idTransferencia, tipo, CCCorigen, CCCdestino, importe, fecha)
        {
            
        }

        public override string ToString()
        {
            return tipo + "\n\t" + base.ToString() + "\t"+ Fecha;
        }
    }
}