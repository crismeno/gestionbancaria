﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTransferencias.Core.Transferencias
{
    using GestionClientes.Core;
    using GestionCuentas;
    public class Periodica : Transferencia
    {
        public const string tipo = "Periodica";

        public Periodica(int idTransferencia, Cuenta CCCorigen, Cuenta CCCdestino, double importe, DateTime fecha)
            : base(idTransferencia, tipo, CCCorigen, CCCdestino, importe, fecha)
        {
        }


        public void Actualizacion()
        {
            i = 1;

            if (Fecha.Day != DateTime.DaysInMonth(Fecha.Year, Fecha.Month))

                if (DateTime.Now.Equals(Fecha.AddMonths(i)))
                {
                    CCCorigen.Saldo = CCCorigen.Saldo - Importe;
                    CCCdestino.Saldo = CCCdestino.Saldo + Importe;
                    i++;
                }

                else

                if (DateTime.Now.Equals(Fecha.AddDays(1).AddMonths(i).AddDays(-1)))
                {
                    CCCorigen.Saldo = CCCorigen.Saldo - Importe;
                    CCCdestino.Saldo = CCCdestino.Saldo + Importe;
                    i++;
                }
        }

        public override string ToString()
        {
            return tipo + "\n\t" + base.ToString() + "\t" + Fecha;
        }

    }

}
