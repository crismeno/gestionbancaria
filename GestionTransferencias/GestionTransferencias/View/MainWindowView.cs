﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace GestionTransferencias.View
{

    

    public class MainWindowView : Form
    {

        public MainWindowView()
        {
            this.Build();
            this.Font = new System.Drawing.Font("Century Gothic", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
        }


        void Build()
        {
            
            this.BuildMenu();
            Panel ListPanel = this.BuildListPanel();

            this.SuspendLayout();

            this.pnlPpal = new Panel()
            {
                Dock = DockStyle.Fill
            };

            this.pnlPpal.SuspendLayout();
            this.Controls.Add(this.pnlPpal);
            this.pnlPpal.Controls.Add(ListPanel);
            this.pnlPpal.ResumeLayout(false);

            this.MinimumSize = new Size(950, 100);
            this.Text = "Transferencias";
            this.Resize += (obj, e) => this.ResizeWindow();

            this.ResizeWindow();
            this.ResumeLayout(true);



        }

        private void ResizeWindow()
        {
            // Tomar las nuevas medidas
            int width = this.pnlPpal.ClientRectangle.Width;

            // Redimensionar la tabla
            this.grdLista.Width = width;

            this.grdLista.Columns[0].Width =
                                (int)System.Math.Floor(width * .05);
            this.grdLista.Columns[1].Width =
                                (int)System.Math.Floor(width * .25);
            this.grdLista.Columns[2].Width =
                                (int)System.Math.Floor(width * .10);
            this.grdLista.Columns[3].Width =
                                (int)System.Math.Floor(width * .15);
            this.grdLista.Columns[4].Width =
                                (int)System.Math.Floor(width * .15);
            this.grdLista.Columns[5].Width =
                                (int)System.Math.Floor(width * .10);
            this.grdLista.Columns[6].Width =
                                (int)System.Math.Floor(width * .20);

            this.grdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;
            this.grdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.PeachPuff;

            var gridCelda = new DataGridViewTextBoxCell();
            var gridCeldaButton = new DataGridViewButtonCell();


            //Diseño de las celdas del grid de la lista
            gridCelda.Style.BackColor = Color.Moccasin;
            gridCelda.Style.ForeColor = Color.Black;

            gridCelda.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //Diseño de las celdas de botones
            gridCeldaButton.Style.BackColor = Color.White;
            gridCeldaButton.Style.ForeColor = Color.Black;
        }

        void BuildMenu()
        {
            this.Mpal = new MainMenu();

            this.MInsertar = new MenuItem("Insertar");
            this.MSalir = new MenuItem("Salir");

            this.OpInserta = new MenuItem("Nueva transferencia");

            this.MInsertar.MenuItems.Add(this.OpInserta);

            this.Mpal.MenuItems.Add(MInsertar);
            this.Mpal.MenuItems.Add(MSalir);


            this.Menu = Mpal;
        }

        Panel BuildListPanel()
        {
            Panel pnlLista = new Panel();
            pnlLista.SuspendLayout();
            pnlLista.Dock = DockStyle.Fill;

            // Crear gridview
            this.grdLista = new DataGridView()
            {
                Dock = DockStyle.Fill,
                AllowUserToResizeRows = false,
                RowHeadersVisible = false,
                AutoGenerateColumns = false,
                MultiSelect = false,
                AllowUserToAddRows = false,
                EnableHeadersVisualStyles = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };

            this.grdLista.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            this.grdLista.ColumnHeadersDefaultCellStyle.BackColor = Color.Moccasin;

            var textCellTemplate0 = new DataGridViewTextBoxCell();
            var textCellTemplate1 = new DataGridViewTextBoxCell();
            var textCellTemplate2 = new DataGridViewTextBoxCell();
            var textCellTemplate3 = new DataGridViewTextBoxCell();
            var textCellTemplate4 = new DataGridViewTextBoxCell();
            textCellTemplate0.Style.BackColor = Color.White;
            textCellTemplate0.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.BackColor = Color.Moccasin;
            textCellTemplate1.Style.ForeColor = Color.Black;
            textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            textCellTemplate2.Style.BackColor = Color.Moccasin;
            textCellTemplate2.Style.ForeColor = Color.Black;
            textCellTemplate3.Style.BackColor = Color.Moccasin;
            textCellTemplate3.Style.ForeColor = Color.Black;
            textCellTemplate4.Style.BackColor = Color.Moccasin;
            textCellTemplate4.Style.ForeColor = Color.Black;

            var col0 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate0,
                HeaderText = "N",
                Width = 30,
                ReadOnly = true
            };

            var col1 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "ID transferencia",
                Width = 30,
                ReadOnly = true
            };

            var col2 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Tipo",
                Width = 30,
                ReadOnly = true
            };

            var col3 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Cuenta origen",
                Width = 20,
                ReadOnly = true
            };

            var col4 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Cuenta destino",
                Width = 50,
                ReadOnly = true
            };

            var col5 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Importe",
                Width = 30,
                ReadOnly = true
            };
            var col6 = new DataGridViewTextBoxColumn
            {
                SortMode = DataGridViewColumnSortMode.NotSortable,
                CellTemplate = textCellTemplate1,
                HeaderText = "Fecha",
                Width = 30,
                ReadOnly = true
            };

            this.grdLista.Columns.AddRange(new DataGridViewColumn[] {
                col0, col1, col2, col3, col4, col5, col6
            });

            pnlLista.Controls.Add(this.grdLista);
            return pnlLista;
        }

        public MainMenu Mpal { get; set; }
        public MenuItem MSalir { get; set; }
        public MenuItem MInsertar { get; set; }
        public MenuItem OpSalir { get; set; }
        public MenuItem OpGuardar { get; set; }
        public MenuItem OpInserta { get; set; }



        public Panel pnlPpal;

        public DataGridView grdLista;
        
    }
}
