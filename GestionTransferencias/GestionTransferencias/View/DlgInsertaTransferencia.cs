﻿using GestionTransferencias.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCuentas.Cuentas;
using GestionCuentas;

namespace GestionTransferencias.View
{
    public class DlgInsertaTransferencia : Form
    {
        public DlgInsertaTransferencia()
        {
            this.Cuentas = new RegistroCuentas().fromXML();
            this.Build();
        }

        private Panel BuildPnlBotones()
        {
            var toret = new TableLayoutPanel()
            {
                ColumnCount = 2,
                RowCount = 1
            };

            var btCierra = new Button()
            {
                DialogResult = DialogResult.Cancel,
                Text = "&Cancelar"
            };

            var btGuarda = new Button()
            {
                DialogResult = DialogResult.OK,
                Text = "&Guardar"
            };

            this.AcceptButton = btGuarda;
            this.CancelButton = btCierra;

            toret.Controls.Add(btGuarda);
            toret.Controls.Add(btCierra);
            toret.Dock = DockStyle.Top;

            return toret;
        }



        Panel buildTipo()
        {
            var toret = new Panel { Dock = DockStyle.Top };
            this.edTipo = new ComboBox { Dock = DockStyle.Right };
            var lbTipo = new Label
            {
                Text = "Tipo:",
                Dock = DockStyle.Left
            };
            edTipo.Anchor = AnchorStyles.Bottom;
            edTipo.DropDownStyle = ComboBoxStyle.DropDownList;
            edTipo.DropDownWidth = 100;
            edTipo.Items.AddRange(new string[] { "Puntual", "Periodica" });
            edTipo.SelectedItem = edTipo.Items[0];


            toret.Controls.Add(edTipo);
            toret.Controls.Add(lbTipo);
            toret.MaximumSize = new Size(400, 100);

            return toret;
        }

        Panel buildCCCorigen()
        {

            var pnlCCCorigen = new Panel { Dock = DockStyle.Top };
            this.edCCCorigen = new ComboBox { Dock = DockStyle.Right };
            var lbCCCorigen = new Label
            {
                Text = "Elige la cuenta de origen: ",
                Dock = DockStyle.Left
            };
            foreach (Cuenta c in Cuentas.cuentas)
            {
                this.edCCCorigen.Items.Add(c.CCC);
            }
            edCCCorigen.Anchor = AnchorStyles.Bottom;
            edCCCorigen.DropDownStyle = ComboBoxStyle.DropDownList;
            edCCCorigen.DropDownWidth = 100;
            if (Cuentas.cuentas.Count >= 1)
            {
                edCCCorigen.SelectedItem = edCCCorigen.Items[0];
            }

            pnlCCCorigen.Controls.Add(edCCCorigen);
            pnlCCCorigen.Controls.Add(lbCCCorigen);

            return pnlCCCorigen;

        }

        Panel buildCCCdestino()
        {
            var pnlCCCdestino = new Panel { Dock = DockStyle.Top };
            this.edCCCdestino = new ComboBox { Dock = DockStyle.Fill };
            var lbCCCdestino = new Label
            {
                Text = "Elige la cuenta destino: ",
                Dock = DockStyle.Left
            };
            foreach (Cuenta c in Cuentas.cuentas)
            {
                this.edCCCdestino.Items.Add(c.CCC);
            }
            edCCCdestino.Anchor = AnchorStyles.Bottom;
            edCCCdestino.DropDownStyle = ComboBoxStyle.DropDownList;
            edCCCdestino.DropDownWidth = 100;
            if (Cuentas.cuentas.Count >= 1)
            {
                edCCCdestino.SelectedItem = edCCCdestino.Items[0];
            }

            pnlCCCdestino.Controls.Add(edCCCdestino);
            pnlCCCdestino.Controls.Add(lbCCCdestino);

            return pnlCCCdestino;
        }

        Panel buildImporte()
        {
            var toret = new Panel();
            this.edImporte = new TextBox
            {
                TextAlign = HorizontalAlignment.Right,
                Dock = DockStyle.Fill
            };

            var lbImporte = new Label
            {
                Text = "Importe: ",
                Dock = DockStyle.Left
            };

            toret.Controls.Add(this.edImporte);
            toret.Controls.Add(lbImporte);
            toret.Dock = DockStyle.Top;
            toret.MaximumSize = new Size(int.MaxValue, edImporte.Height);



            return toret;
        }




        void Build()
        {
            this.SuspendLayout();

            var panelInserta = new TableLayoutPanel { Dock = DockStyle.Fill };
            panelInserta.SuspendLayout();
            this.Controls.Add(panelInserta);

            var panelTipo = this.buildTipo();
            panelInserta.Controls.Add(panelTipo);
            var panelCCCorigen = this.buildCCCorigen();
            panelInserta.Controls.Add(panelCCCorigen);
            var panelCCCdestino = this.buildCCCdestino();
            panelInserta.Controls.Add(panelCCCdestino);
            var panelImporte = this.buildImporte();
            panelInserta.Controls.Add(panelImporte);
            var panelBotones = this.BuildPnlBotones();
            panelInserta.Controls.Add(panelBotones);

            panelInserta.ResumeLayout(true);

            this.Text = "Nueva transferencia";
            this.Size = new Size(400,
                                panelTipo.Height
                            + panelCCCorigen.Height
                            + panelCCCdestino.Height + panelImporte.Height + panelBotones.Height);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterParent;
            this.ResumeLayout(false);
        }

        private ComboBox edTipo;
        private ComboBox edCCCorigen;
        private ComboBox edCCCdestino;
        private TextBox edImporte;



        public string Tipo => this.edTipo.Text;
        public ulong CCCorigen => System.Convert.ToUInt64(this.edCCCorigen.Text);
        public ulong CCCdestino => System.Convert.ToUInt64(this.edCCCdestino.Text);
        public double Importe => System.Convert.ToDouble(this.edImporte.Text);

        private RegistroCuentas Cuentas { get; set; }
    }
}

