﻿using GestionTransferencias.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionCuentas;

namespace GestionTransferencias.View
{
    class MainWindowCore
    {
        public MainWindowCore()
        {
            this.registroCuentas = new RegistroCuentas().fromXML();
            this.registro = RegistroTransferencias.RecuperarXml(this.registroCuentas);

            this.MainWindowsView = new MainWindowView();

            this.MainWindowsView.OpInserta.Click += (sender, e) => this.Insertar();
            this.MainWindowsView.Shown += (sender, e) => this.Actualiza();
            this.MainWindowsView.Closed += (sender, e) => this.Salir();
        }

        void Insertar()
        {
            var dlgInserta = new DlgInsertaTransferencia();

            if (dlgInserta.ShowDialog() == DialogResult.OK)
            {
                Transferencia trans = Transferencia.Crea(dlgInserta.Tipo, registroCuentas.getCuenta(dlgInserta.CCCorigen), registroCuentas.getCuenta(dlgInserta.CCCdestino), dlgInserta.Importe);
                this.registro.Add(trans);
            }
            this.Actualiza();
        }

        

        void Actualiza()
        {
            int numElementos = this.registro.Count();
            for (int i = 0; i < numElementos; i++)
            {
                if (this.MainWindowsView.grdLista.Rows.Count <= i)
                {
                    this.MainWindowsView.grdLista.Rows.Add();
                }
                this.ActualizarFilaLista(i);
            }

            // Eliminar filas sobrantes
            int numExtra = this.MainWindowsView.grdLista.Rows.Count - numElementos;
            for (; numExtra > 0; --numExtra)
            {
                this.MainWindowsView.grdLista.Rows.RemoveAt(numElementos);
            }
        }

        private void ActualizarFilaLista(int numFila)
        {
            if (numFila < 0
              || numFila > this.MainWindowsView.grdLista.Rows.Count)
            {
                throw new System.ArgumentOutOfRangeException(
                            "fila fuera de rango: " + nameof(numFila));
            }

            DataGridViewRow fila = this.MainWindowsView.grdLista.Rows[numFila];
            Transferencia trans = this.registro[numFila];



            fila.Cells[0].Value = (numFila + 1).ToString().PadLeft(4, ' ');
            fila.Cells[1].Value = trans.IdTransferencia;
            fila.Cells[2].Value = trans.Tipo;
            fila.Cells[3].Value = trans.CCCorigen.CCC;
            fila.Cells[4].Value = trans.CCCdestino.CCC;
            fila.Cells[5].Value = trans.Importe;
            fila.Cells[6].Value = trans.Fecha;

            foreach (DataGridViewCell celda in fila.Cells)
            {
                celda.ToolTipText = trans.ToString();
            }

        }

        void Salir()
        {
            this.registro.GuardarXml();
            Application.Exit();
        }

        public MainWindowView MainWindowsView
        {
            get; private set;
        }
        public RegistroTransferencias registro;

        public RegistroCuentas registroCuentas;
    }


}